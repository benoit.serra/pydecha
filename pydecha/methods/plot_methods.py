# -*- coding: utf-8 -*-
"""
Methods for PYDECHA

Created on Wed May  8 17:19:00 2019

@author: bserra

Reminder
----------
_PEP 484:
https://www.python.org/dev/peps/pep-0484/
----------
"""
# IMPORTS
#########################
import logging
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 16})
import numpy as np
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib
matplotlib.rcParams['mathtext.fontset'] = 'stix'
matplotlib.rcParams['font.family'] = 'STIXGeneral'


# METHODS
#########################
def stats_array(array):
    """
    """
    mu = np.mean(array)
    std = np.std(array)
    med = np.median(array)
    str_stats = "mean=" + str(mu) + "\n" +\
                "std=" + str(std) + "\n" +\
                "median=" + str(std)

    stats = {'mean': mu,
             'std': std,
             'median': med,
             'label': str_stats}

    return stats


def colorbar(mappable, scale=None):
    """
    """
    ax = mappable.axes
    fig = ax.figure
    divider = make_axes_locatable(ax)
    if scale is None:
        cax = divider.append_axes('right', size='5%', pad=0.05)
    else:
        cax = divider.append_axes('right', size=str(scale)+'%', pad=0.05)

    return fig.colorbar(mappable, cax=cax)


def tag_plot(fig, infos):
    """"""
    text = "\n".join(['File location: '+infos['general']['savepath'],
                      'Detector: '+infos['detector']['type'],
                      'Test bench: '+infos['general']['setup'],
                      'pydecha vers. '+infos['general']['version'],
                      'pydecha mod.: '+__file__])
    # 'pydecha mod.: '+str(infos['module_name'])])
    plt.text(0.01, 0.01, text,
             horizontalalignment='left',
             verticalalignment='bottom',
             transform=fig.transFigure)

    text = "\n".join([key+': '+str(value) for key, value in infos.items() if type(value) is not type(dict())])
    # text = "\n".join(['mod. params: '+str(infos['module_params'])])
    plt.text(0.01, 0.99, text,
             horizontalalignment='left',
             verticalalignment='top',
             transform=fig.transFigure)


def params_plot(infos, fig):
    """"""
    text = "\n".join(['mod. params: '+str(infos['module_params'])])
    plt.text(0.01, 0.99, text,
             horizontalalignment='left',
             verticalalignment='top',
             transform=fig.transFigure)


def savefig(infos, name):
    """"""
    plt.savefig(infos['savepath']+"/"+infos['tstart']+'_'+name,
                metadata=infos)


def set_ax(ax, dict_args):
    """
    """
    ax.set_xlabel(dict_args['x_label'])
    ax.set_ylabel(dict_args['y_label'])
    ax.set_title(dict_args['title'])
    ax.grid(True)


def fitline_plot(x_array, y_array, **kwargs):
    """
    """
    fig, ax = plt.subplots(2, 1, figsize=(20, 15))
    ax[0].plot(x_array, y_array, 'bo')
    if 'fit' in kwargs.keys():
        fit_par = kwargs['fit']
        fit_lim = slice(fit_par['fit_start'], fit_par['fit_stop'])
        p = np.polyfit(x_array[fit_lim],
                       y_array[fit_lim], 1)
        fit = np.poly1d(p)
        ax[0].plot(x_array[fit_lim], fit(x_array[fit_lim]), '-k',
                   label=str('{:1.3f}'.format(p[0]))+r' $'+fit_par['fit_unit']+'$')
        # Error in respect to the residuals from the fit
        residuals = ((fit(x_array)-y_array)/fit(x_array))*100
    ptop = np.max(residuals)-np.min(residuals)
    ax[1].plot(x_array, residuals, 'bo',
               label='Residuals\n' +
               str('{:1.3f}'.format(ptop))+'% non-linearity')
    set_ax(ax[0], kwargs)
    ax[1].set_ylabel('Residuals')
    ax[1].grid(True)
    [axe.legend() for axe in fig.get_axes()]

    return fig, ax, '{:1.3f}'.format(p[0])


def show_map(data_array, **kwargs):
    """Function verifies a module structure
    Args:
        data_array (np.array): 2D array representing the map the user wants
        to display

    Returns:
        Nothing.
    """
    fig, axes = plt.subplots(1, 2, figsize=(20, 15))
    im = axes[0].imshow(data_array,
                        vmin=np.percentile(data_array, kwargs['min_perc']),
                        vmax=np.percentile(data_array, kwargs['max_perc']),
                        cmap='binary')

    vmin, vmax = np.percentile(data_array, kwargs['min_perc']), np.percentile(data_array, kwargs['max_perc'])

    axes[1].hist(data_array.flatten(),
                 bins=np.arange(vmin, vmax, (vmax-vmin)/100),
                 histtype='step',
                 lw=2)

    axes[0].set_title(kwargs['title'], fontsize=14)
    axes[1].yaxis.tick_right()
    axes[1].set_xlabel('Dark current [e-/s]')
    axes[1].grid(True)
    cb = colorbar(im)
    cb.ax.set_ylabel('Dark current [e-/s]')

    for ax in fig.get_axes():
        plt.setp(ax.get_xticklabels(), fontsize=14)
        plt.setp(ax.get_yticklabels(), fontsize=14)

    plt.subplots_adjust(left=0,
                        bottom=0.2,
                        right=0.9,
                        top=0.93,
                        wspace=0.0,
                        hspace=0.2)

    return fig


# MAIN
#########################
if __name__ == "__main__":
    pass


# GARBAGE
#########################
