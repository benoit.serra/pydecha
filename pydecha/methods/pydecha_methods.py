# -*- coding: utf-8 -*-
"""
Methods for PYDECHA

Created on Wed May  8 17:19:00 2019

@author: bserra

Reminder
----------
_PEP 484:
https://www.python.org/dev/peps/pep-0484/
----------
"""
# IMPORTS
#########################
import os
import sys
import glob
import logging
import time
import shutil
from tqdm import tqdm

import argparse
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.axes_grid1 import make_axes_locatable
import importlib


# METHODS
#########################
def read_yamlreport(filename, **kwargs):
    """Method for reading a YAML file.

    Args:
        filename (int): path to the file.

    Returns:
        yaml_report (dict): The return value. Dictionnary

    """
    import yaml
    logger = logging.getLogger('pydecha_methods')
    # ERR_DICT = kwargs["errors"]
    yaml_report = None
    try:
        with open(filename, 'r') as stream:
            try:
                yaml_report = yaml.load(stream, Loader=yaml.UnsafeLoader)
            except yaml.YAMLError as exc:
                logger.critical('YAML not opening, check YAML formatting', exc)
    except (IOError, RuntimeError, TypeError, NameError) as exc:
        logger.critical('YAML file not available', exc)

    return yaml_report


def create_module(detector_type, module_name):
    """Function that will create an empty module for pydecha with every file
    that is needed

    Args:
        module_name (str): user defined module's name

    Returns:
        Nothing.
    """
    logger = logging.getLogger('pydecha_methods')

    # Is not working on UNIX AND Windows if I do not use os.path.abspath
    path = os.path.abspath(os.getcwd()+'/pydecha/modules/'+detector_type)
    template_module = '_TEMPLATE'

    # Copying the template with the user defined module_name instead
    src = path+'/'+template_module
    dest = path+'/'+module_name
    
    try:
        os.mkdir(dest)
        # Replacing all of template in filenames and directories by module_name
        for dirpath, subdirs, files in os.walk(src):
            # print (dirpath, subdirs, files)

            for x in files:
                pathtofile = os.path.join(dirpath, x)
                new_pathtofile = pathtofile.replace(
                        template_module, module_name)
                shutil.copy(pathtofile,
                            new_pathtofile)
                # Open file in the created copy
                with open(new_pathtofile, 'r') as file_tochange:
                    # Replace any mention of template by module_name
                    new_contents = file_tochange.read().replace(
                            template_module, module_name)
                    new_contents = new_contents.replace(
                            '%(date)', time.ctime())
                with open(new_pathtofile, 'w+') as file_tochange:
                    file_tochange.write(new_contents)
                # Close the file other we can't rename it
                file_tochange.close()

            for x in subdirs:
                pathtofile = os.path.join(dirpath, x)
                os.mkdir(pathtofile.replace(template_module, module_name))
            logger.info('Module '+module_name+' created.')
        print('Module '+module_name+' created in '+path+'.')
    # Directories are the same
    except shutil.Error as e:
        logger.critical('Error while duplicating '+template_module+': %s' % e)
    # Any error saying that the directory doesn't exist
    except OSError as e:
        logger.critical(module_name+' not created. Error: %s' % e)

    return None


def create_config():
    """Function that will create a configuration file using the template in
    pydecha/templates/config/pydecha_config.yaml
    """
    read_yamlreport('pydecha/templates/config/pydecha_config.yaml', **kwargs)


def _get_systeminfos():
    """Function to get system informations and return a dictionnary
    :return sys_infos (dict): dictionnary with system infos from platform.uname
    """
    import platform

    sys_infos = dict()
    # keywords for platform.uname list:
    # (system, node, release, version, machine, processor)
    uname = platform.uname()
    sys_infos.update({'python': {
                          'build':   platform.python_build(),
                          'version': platform.python_version()
                          }
                      })
    sys_infos.update({'host': {
                          'hostname': uname.node,
                          'system': uname.system,
                          'release': uname.release,
                          'version': uname.version,
                          'machine': uname.machine,
                          'processor': uname.processor
                          }
                      })
    return sys_infos


def _verify_module():
    """Function verifies a module structure
    Args:
        module_name (str): user defined module's name

    Returns:
        Nothing.
    """
    return None


def _import_modules(detector_type):
    """"""
    logger = logging.getLogger('pydecha_methods')
    # All slashes otherwise it doesn't work on unix machines
    path = os.getcwd()+"/pydecha/modules/"+detector_type+"/"
    path = os.path.abspath(path)
    available_modules = glob.glob(path+'/*')
    for module in available_modules:
        module_name = os.path.basename(module)
        if ('_TEMPLATE' not in module_name) & ('__init__' not in module_name) & ('__pycache__' not in module_name):
            sys.path.insert(0, module)
            # Here I import all modules from pydecha of this particular pipeline
            importlib.import_module(os.path.basename(module)+"_analysis")
            try:
                importlib.import_module(os.path.basename(module)+"_acquisition")
            except ModuleNotFoundError:
                logger.info(f'Module {module} not found')


def _update_modules2(settings_dict):
    """"""
    logger = logging.getLogger('pydecha_methods')
    import shutil
    detector_type = settings_dict['workflow']['detector']['type']
    doc_path = f'docs/source/modules/modules_{detector_type}/'
    shutil.rmtree(doc_path)
    os.mkdir(doc_path)
    with open(f'docs/source/modules/modules_{detector_type}.rst', 'w') as module_doc:
        name = 'Modules '+detector_type.upper()
        module_doc.write(name+'\n'+'*'*len(name)+'\n\n')
        module_doc.write('.. toctree::\n\n')
        if settings_dict['workflow']['list'] is not None:
            modules = settings_dict['workflow']['list'].keys()
            pbar = tqdm(modules, ascii=True, ncols=80)
            pbar.set_description('Creating module doc')
            for idx, module in enumerate(pbar):
                module_rst = open(f'docs/source/modules/modules_{detector_type}/{module}.rst','w')
                module_rst.write(f'{module}\n')
                module_rst.write('*'*len(module)+'\n\n')
                module_doc.write(f'   modules_{detector_type}/{module}\n\n')
                for module_type in ['acquisition','analysis']:
                    try:
                        module_path = settings_dict[module_type]['list'][f'{module}']['module_name']
                        module_name = '.'.join(module_path.__file__.split('\\')[-2:]).replace('.py', '')
                        #module_rst = open(f'docs/source/modules/modules_{detector_type}/{module_name}.rst','w')
                        module_string = f'.. automodule:: {detector_type}.{module_name}\n   :members:\r\n'
                        module_rst.write(module_string)
                    except Exception as E:
                        logger.info(E)
                        
    module_doc.close()
    try:
        shutil.rmtree('pydecha/modules/'+detector_type+'/__pycache__')
    except Exception as E:
        print (E)
    return None


def _update_modules(setting_dict):
    """"""
    import shutil
    detector_type = setting_dict['workflow']['detector']['type']
    with open('docs/source/modules/modules_'+detector_type+'.rst', 'w') as module_doc:
        name = 'Modules '+detector_type.upper()
        module_doc.write(name+'\n'+'*'*len(name)+'\n\n')
        if setting_dict['analysis']['list'] is not None:
            modules = setting_dict['analysis']['list'].items()
            pbar = tqdm(modules, ascii=True, ncols=60)
            pbar.set_description('Creating module doc')
            for idx, module in enumerate(pbar):
                module_path = module[1]['module_name']
                module_name = '.'.join(module_path.__file__.split('\\')[-2:]).replace('.py', '')
                module_string = '.. automodule:: '+detector_type+'.'+module_name+'\n   :members:\r\n'
                module_doc.write(module_string)
                if idx != len(modules)-1:
                    module_doc.write(72*'-'+'\n\n')
    module_doc.close()
    shutil.rmtree('pydecha/modules/'+detector_type+'/__pycache__')

def _update_version(version_number):
    """"""
    # Open file in the created copy
    with open('docs/source/conf_template.py', 'r') as file_tochange:
        # Replace any mention of template by module_name
        new_contents = file_tochange.read().replace(
                "version = '0.1'", "version = '"+version_number+"'")
        new_contents = new_contents.replace(
                "release = '0.1'", "release = '"+version_number+"'")
    with open('docs/source/conf.py', 'w+') as file_tochange:
        file_tochange.write(new_contents)
    # Close the file other we can't rename it
    file_tochange.close()

# CLASSES
#########################
class pipelines(object):

    def __init__(self):
        self._tinit = time.time()

    def start():
        # acquisition = pdc_acq()
        # analysis = pdc_ana()
        # reporting = pdc_rep()
        return None


# MAIN
#########################
if __name__ == "__main__":
    pass


# GARBAGE
#########################
