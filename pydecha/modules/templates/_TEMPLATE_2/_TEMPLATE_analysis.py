# -*- coding: utf-8 -*-
"""
Created on %(date)

@author: bserra
@analysis: _TEMPLATE

Reminder
----------
_PEP 484:
https://www.python.org/dev/peps/pep-0484/
----------
"""
# IMPORTS
#########################
import logging


# METHODS
#########################
def start(settings__TEMPLATE):
    """Example function with types documented in the docstring.

    Args:
        settings__TEMPLATE (dict): Input parameters for the analysis.

    Returns:
        None: The return value. Nothing for now...

    """
    logger = logging.getLogger('_TEMPLATE_analysis')
    logger.info('Starting _TEMPLATE analysis...')
    # _TEMPLATE code goes here
    logger.info('Ending _TEMPLATE analysis...')

    return None


# MAIN
#########################
if __name__ == "__main__":
    pass


# GARBAGE
#########################
