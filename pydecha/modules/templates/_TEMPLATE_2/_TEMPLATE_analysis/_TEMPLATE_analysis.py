# -*- coding: utf-8 -*-
"""
Created on %(date)

@author: bserra
@analysis: _TEMPLATE

Reminder
----------
_PEP 484:
https://www.python.org/dev/peps/pep-0484/
----------
"""
# IMPORTS
#########################
import sys

# METHODS
#########################
def _TEMPLATE(param1, param2):
    """Example function with types documented in the docstring.

    Args:
        param1 (int): The first parameter.
        param2 (str): The second parameter.

    Returns:
        bool: The return value. True for success, False otherwise.

    """
    return None

# MAIN
#########################
if __name__ == "__main__":
    pass


# GARBAGE
#########################
