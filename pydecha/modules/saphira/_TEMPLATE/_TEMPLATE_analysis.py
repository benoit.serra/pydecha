# -*- coding: utf-8 -*-
"""
_TEMPLATE analysis module
-------------------------------------------------------------------------------

+--------------+----------------------------------+---------------------------+
| Author       | Name                             | Creation                  |
+--------------+----------------------------------+---------------------------+
| Benoit Serra | _TEMPLATE                        | %(date)                   |
+--------------+----------------------------------+---------------------------+

+-----------------+-------------------------------------+---------------------+
| Contributor     | Name                                | Creation            |
+-----------------+-------------------------------------+---------------------+
| Name            | filename                            | 06/21/2019          |
+-----------------+-------------------------------------+---------------------+

This is a documentation template for the _TEMPLATE analysis module.
Documentation is generated going into docs/ and typing 'make html'

This module can be found in pydecha/modules/_TEMPLATE/_TEMPLATE_analysis.py.
Please modify the docstrings accordingly to provide the users a simple and
detailed explanation of your algorithm for this module.

Table examples
==============

===============  ==============================================================
Table entries    Table values
===============  ==============================================================
Entry 1          Value 1
Entry 2          Value 2
Entry 3          Value 3

Entry 4          Value 4

Entry 5          Value 5
===============  ==============================================================

+------------------------+------------+----------+----------+
| Header row, column 1   | Header 2   | Header 3 | Header 4 |
| (header rows optional) |            |          |          |
+========================+============+==========+==========+
| body row 1, column 1   | column 2   | column 3 | column 4 |
+------------------------+------------+----------+----------+
| body row 2             | Cells may span columns.          |
+------------------------+------------+---------------------+
| body row 3             | Cells may  | - Table cells       |
+------------------------+ span rows. | - contain           |
| body row 4             |            | - body elements.    |
+------------------------+------------+----------+----------+
| body row 5             | Cells may also be     |          |
|                        | empty: ``-->``        |          |
+------------------------+-----------------------+----------+

Code example
============

.. code-block:: python

    import sys

    print('Hello world...')


.. literalinclude:: ../../../pydecha/modules/cmos/_TEMPLATE/_TEMPLATE_analysis.py
    :language: python
    :linenos:
    :lines: 84-87

Useful links
============

ReadTheDocs documentation
https://sphinx-rtd-theme.readthedocs.io/en/latest/index.html

.. todo::

   Write the documentation for _TEMPLATE
"""
# IMPORTS
#########################
# PYDECHA specific modules and classes
import methods.plot_methods as plotm
from classes.analysis_module import analysis
# Standard modules
import matplotlib.pyplot as plt
import glob as glob
import numpy as np
from tqdm import tqdm

# METHODS & CLASSES
#########################
class module(analysis):
    """Example class with documented types

    :param None: analysis module initialize with the parameters from the yaml conf. file

    :return None: The return value. Nothing for now...
    """
    def start(self):
        """"""
        # logger is the logger initialized in analysis_module.analysis
        # depending of the severity of the error the user can use:
        # logger.info/warning/error/critical
        logger = self.logger
        logger.info('Starting _TEMPLATE analysis...')
        # _TEMPLATE code goes here
        print('_TEMPLATE analysis module')
        print('_TEMPLATE parameters')
        # self.module_parameters is a dictionnary containing the module information
        # that were written in the .yaml configuration file
        print(self.module_parameters)
        file_list = glob.glob(self.module_parameters+'*.fits')
        self.get_dataset_info(file_list,
                              file_type='FITS',
                              save=True)

        # _TEMPLATE analysis
        # Creating an empty dataset
        data_array = np.zeros((1000, 1000))
        # Creating a figure
        fig, ax = plt.subplots(1, 1)
        # If analysis is done within a loop in the files, tqdm can be used
        # to display a progress bar
        for i in tqdm(range(10000), ascii=True, ncols=60):
            pass
        # saving a parameter can be done using self.save_param
        self.save_param('_TEMPLATE variable name', 'Variable value')
        # saving a figure in
        # 'outputs/YYYYMMDD_DETECTOR/YYYYMMDD__TEMPLATE-figurename.png'
        self.save_fig(fig, 'figurename')
        # saving a map to FITS, all information in module_parameters will
        # be put in the header
        self.save_map(data_array,
                      metadata=self.module_parameters, file_type='FITS')

        logger.info('Ending _TEMPLATE analysis...')
        # if self.information was updated (for example using self.save_param)
        # returning it will save this info in the output yaml file.
        return self.clean_output()


# MAIN
#########################
if __name__ == "__main__":
    pass


# GARBAGE
#########################
