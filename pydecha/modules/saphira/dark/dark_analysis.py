# -*- coding: utf-8 -*-
"""
dark analysis module
-------------------------------------------------------------------------------

+--------------+----------------------------------+---------------------------+
| Author       | Name                             | Creation                  |
+--------------+----------------------------------+---------------------------+
| Benoit Serra | dark                             | Sun Jun  7 16:09:08 2020  |
+--------------+----------------------------------+---------------------------+

+-----------------+-------------------------------------+---------------------+
| Contributor     | Name                                | Creation            |
+-----------------+-------------------------------------+---------------------+
| Leander Mehrgan | Dark_loop_measuremnt_cube.py        | 02/07/2020          |
+-----------------+-------------------------------------+---------------------+

This is a documentation template for the dark analysis module using Saphira detectors.
Documentation is generated going into docs/ and typing 'make html'

This module can be found in pydecha/modules/saphira/dark/dark_analysis.py.
Please modify the docstrings accordingly to provide the users a simple and
detailed explanation of your algorithm for this module.

Table examples
==============

===============  ==============================================================
Table entries    Table values
===============  ==============================================================
Entry 1          Value 1
Entry 2          Value 2
Entry 3          Value 3

Entry 4          Value 4

Entry 5          Value 5
===============  ==============================================================

+------------------------+------------+----------+----------+
| Header row, column 1   | Header 2   | Header 3 | Header 4 |
| (header rows optional) |            |          |          |
+========================+============+==========+==========+
| body row 1, column 1   | column 2   | column 3 | column 4 |
+------------------------+------------+----------+----------+
| body row 2             | Cells may span columns.          |
+------------------------+------------+---------------------+
| body row 3             | Cells may  | - Table cells       |
+------------------------+ span rows. | - contain           |
| body row 4             |            | - body elements.    |
+------------------------+------------+----------+----------+
| body row 5             | Cells may also be     |          |
|                        | empty: ``-->``        |          |
+------------------------+-----------------------+----------+

Code example
============

.. code-block:: python

    import sys

    print('Hello world...')


.. literalinclude:: ../../../pydecha/modules/cmos/dark/dark_analysis.py
    :language: python
    :linenos:
    :lines: 84-87

Useful links
============

ReadTheDocs documentation
https://sphinx-rtd-theme.readthedocs.io/en/latest/index.html

.. todo::

   Write the documentation for dark
"""
# IMPORTS
#########################
# PYDECHA specific modules and classes
import methods.plot_methods as plotm
from classes.analysis_module import analysis
# Standard modules
import os
import matplotlib.pyplot as plt
import glob as glob
import numpy as np
from tqdm import tqdm
from astropy.io import fits
import pandas as pd
from scipy.stats import linregress

# METHODS & CLASSES
#########################
class module(analysis):
    """Example class with documented types

    :param None: analysis module initialize with the parameters from the yaml conf. file

    :return None: The return value. Nothing for now...
    """
    def start(self):
        """"""
        def GetValue(S, ValueS, Header):
            if len(Header) > 0:
                value= img_header['SEQ1 DIT']
            else:
                # l = len(ValueS)
                # pre = S.find(ValueS)
                pre = S.find(ValueS) + len(ValueS)
                after = S.find('_',pre)
                value = float(S[pre:after])
                # print('From filename:',value)
            return value
        
        # logger is the logger initialized in analysis_module.analysis
        # depending of the severity of the error the user can use:
        # logger.info/warning/error/critical
        logger = self.logger
        logger.info('Starting dark analysis...')
        # dark code goes here
        print('dark analysis module')
        print('dark parameters')
        # self.module_parameters is a dictionnary containing the module information
        # that were written in the .yaml configuration file
        print(self.module_parameters)

        parameters = ['HIERARCH ESO DET SEQ1 DIT',
                      'HIERARCH ESO DET CLDC1 DC9',
                      'HIERARCH ESO DET CLDC1 DC5',
                      'HIERARCH ESO DET CLDC1 DC6']

        file_list = glob.glob(self.module_parameters['data_path']+'*.fits')
        self.get_dataset_info(file_list,
                              file_type='FITS',
                              save=True,
                              parameters=parameters)
        # dark analysis

        # Parameters from the configuration file
        analysis_window = self.module_parameters['subarray']['enable']

        window_x = slice(self.module_parameters['subarray']['start_x'],
                         self.module_parameters['subarray']['start_x']+self.module_parameters['subarray']['width_x'])
        window_y = slice(self.module_parameters['subarray']['start_y'],
                         self.module_parameters['subarray']['start_y']+self.module_parameters['subarray']['width_y'])

        table_start = self.module_parameters['n_param']['n1']
        table_stop = self.module_parameters['n_param']['n2']

        skip_files = self.module_parameters['skipfiles']

        fit_limits = slice(self.module_parameters['fit']['fit_start'],
                           self.module_parameters['fit']['fit_stop'])

        results = np.array([])
        # For each fits file in the data path
        for file_idx, filename in enumerate(tqdm(file_list, ascii=True, ncols=60)):
            # Get the data and the infos
            img_data, img_header = fits.getdata(filename, header=True)
            # Extract some keywords
            DIT_value = img_header['ESO DET SEQ1 DIT']
            COM_value = img_header['ESO DET CLDC1 DC9']
            VDDOP_value = img_header['ESO DET CLDC1 DC5']
            PRV_value = img_header['ESO DET CLDC1 DC6']
            TDETvalue = GetValue(filename, "TDET_", "")
            Preamp_value = GetValue(filename, "PreampSV_", "")			

            nb_frames = len(img_data)
            # Create an empty dataframe that will contain the time of the frame
            # and its spatial average
            utr_dataframe = pd.DataFrame([],columns=['Time', 'Spatial average'])

            # For each frame, get the spatial average of the window/full frame
            for idx, frame in enumerate(tqdm(img_data, ascii=True, ncols=60)):
                if analysis_window:
                    img = frame[window_x, window_y]
                else:
                    img = frame
                # And append it to the dataframe
                utr_dataframe.loc[idx] = [float(idx) * DIT_value, np.mean(img)]
            # Sort the dataframe by Time
            utr_dataframe.sort_values(['Time'])

            # Indexes of the dataframe
            a = np.arange(len(utr_dataframe['Time']))


            df_filtered = utr_dataframe[(a%skip_files == 0)]
            linreg_coefs = linregress(df_filtered['Time'][fit_limits],
                                      df_filtered['Spatial average'][fit_limits])

            fig, ax = plt.subplots(figsize=(15, 10))
    
            fig.subplots_adjust(top=0.85)
            fig.subplots_adjust(bottom=0.3)
            #ax.yaxis.set_major_formatter(ticker.StrMethodFormatter("{x:2.2e}"))
            #Plot_title = Plot_title + ("TDET {:1.2f}K, VDD {:1.2f}V, COM {:1.2f}V, preamp {:1.2f}V"
            #               .format(TDETvalue, VDDOP_value, COM_value,  Preamp_value))
            # os.basename otherwise I get the whole path as well.
            ax.set_title(os.path.basename(filename))
            #ax.set_xlabel(plot_xlabel, fontdict=font)
            #ax.set_ylabel(plot_ylabel, fontdict=font)
            ax.plot(utr_dataframe['Time'],
                    utr_dataframe['Spatial average'],
                    'b.', label='Spatial average UTR frames')
            ax.plot(df_filtered['Time'],
                    df_filtered['Spatial average'],
                    'rx', label='Spatial average UTR frames\nNumber of skipped files: '+str(skip_files))
            ax.plot(df_filtered['Time'],
                    linreg_coefs.intercept + linreg_coefs.slope*df_filtered['Time'],
                    'g-', label='Fitted line\n'+str('{:1.3f}'.format(linreg_coefs.slope)))
            ax.grid(True, color='gray', alpha=0.5)
            ax.legend(fontsize=14)
            self.save_fig(fig, os.path.basename(filename))
            plt.close()

            results = np.append(results, [filename,
                                          DIT_value,
                                          COM_value,
                                          VDDOP_value,
                                          PRV_value,
                                          TDETvalue,
                                          Preamp_value,
                                          linreg_coefs.slope])

        #np.savetxt('results_dark', results, delimiter='\t')
        # saving a parameter can be done using self.save_param
        self.save_param('dark variable name', 'Variable value')

        # saving a figure in
        # 'outputs/YYYYMMDD_DETECTOR/YYYYMMDD_dark-figurename.png'
        # saving a map to FITS, all information in module_parameters will
        # be put in the header, beware of paramters that are too long
        # deactivated for saphira as path is too long for FITS header
        #self.save_map(data_array,
        #              metadata=self.module_parameters, file_type='FITS')

        logger.info('Ending dark analysis...')
        # if self.information was updated (for example using self.save_param)
        # returning it will save this info in the output yaml file.
        return self.clean_output()


# MAIN
#########################
if __name__ == "__main__":
    pass


# GARBAGE
#########################
