# -*- coding: utf-8 -*-
"""
noise analysis module
-------------------------------------------------------------------------------

+--------------+----------------------------------+---------------------------+
| Author       | Name                             | Creation                  |
+--------------+----------------------------------+---------------------------+
| Benoit Serra | noise                            | Mon Jul 15 13:52:52 2019  |
+--------------+----------------------------------+---------------------------+

+-----------------+-------------------------------+---------------------------+
| Author          | Name                          | Creation                  |
+-----------------+-------------------------------+---------------------------+
| Benoit Serra    | results_nghxrg.ipynb          | 07/15/2019                |
+-----------------+-------------------------------+---------------------------+

This module can be found in pydecha/modules/noise/noise_analysis.py.
Please modify the docstrings accordingly to provide the users a simple and
detailed explanation of your algorithm for this module.

Noise
=====

The noise most of the times refer to the read-out noise of the system. Which means
the incertainty of the measure of the number of photo-generated charges on the pixel.
Unit is usually in electrons.

Data acquisition
================

To acquire data for obtaining a proper noise map, the detector must be in low
signal condition (dark) and a Correlated Double Sampling (CDS) is acquired.

Computation
===========

The computation consist of deriving the usual estimators using the CDS frame:
mean, median, standard deviation.

One other option is to plot the Power Spectral Density (PSD).
Using the CDS frame, separate the data by respective channels and flatten the data
to obtain a 1D signal representing the pixels value in respect to time.
Using the sample rate of the detector (workflow/detector/readout), it is then
possible to compute a PSD for these channels.

Plots
=====

Noise map
#########

A simple image with the noise distribution and the associated histogram and
statistics

Power Spectral Density(ies)
###########################

PSD of the channels (average) or PSDs of channels taken individually.

Analysis block
==============
"""
# IMPORTS
#########################
# PYDECHA specific methods and classes
import methods.plot_methods as plotm
from classes.analysis_module import analysis
import modules.cmos.noise.noise_plots as noise_plots
import matplotlib.pyplot as plt
from tqdm import tqdm # usually with tqdm(iterable, ascii=True, ncols=60)
from astropy.io import fits
import numpy as np
from scipy import signal
import glob

# METHODS & CLASSES
#########################
class module(analysis):
    """Noise analysis block

    :param module_name (str): Name of the module as string.
    :param yaml_dict (dict): full dictionnary of the config file.

    :return clean_output(): a cleaned dictionnary with the parameters saved

    The noise analysis requires the following parameters in the configuration
    file:

    :param data_path (str): absolute path of where the data is.
    :param nperseg (int): cutoff value for the outliers (generally cutoff*std).
    :param channels (str): to choose the display of the PSD ['all': one PSD per
                           output, 'average': average PSDs of all channels]

    In addition, for this module, some parameters where extract from detector
    properties. They should be accessible via the FITS header

    .. todo::

      Rely more on FITS header data (especially for dimensions). There is no
      information available for readout speed and number of channels.
    """
    def start(self):
        """"""
        # logger is the logger initialized in analysis_module.analysis
        logger = self.logger
        logger.info('Starting noise analysis...')
        print('Noise analysis module')
        # noise code goes here
        # Module parameters entered in the yaml file for the module
        # self.module_parameters
        # General information that are used for tagging plots usually
        # self.information
        # Detector properties defined in the yaml file
        # self.detector_parameters
        # savepath for the data
        # self.savepath

        data_path = self.module_parameters['data_path']
        noise_files = glob.glob(data_path+'/*.fits')
        print (noise_files)
        #self.get_dataset_info(noise_files,
        #                      file_type='FITS',
        #                      save=True)
        filename = noise_files[0]
        file_hdus = fits.open(filename)
        # In real files, noise_infos are not present (only Raushcher simulation)
        # noise_infos = get_noise_params(file_hdus[0].header)
        data = file_hdus[0].data[0].astype(np.float32)

        # Conversion gain
        conversion_gain = 1
        data_corr = data * conversion_gain

        """
        For power spectra, need to be careful of readout directions

        For the periodogram, using Welch's method to smooth the PSD
        > Divide data in N segment of length nperseg which overlaps at nperseg/2 (noverlap argument)
        nperseg high means less averaging for the PSD but more points
        """

        nb_output = self.module_parameters['detector']['nb_outputs']
        dimension = self.module_parameters['detector']['dimensions']
        read_freq = self.module_parameters['detector']['read_freq']
        nperseg = self.module_parameters['nperseg']
         
        """
        dataset = []
        for pix_channel in range(int(256/32)):
            dataset.append(data_corr[:,pix_channel*32])
        
        dataset = np.array(dataset)
        """
        PSDs = []
        for i in range(256):
            f_vect, P1_outputs = signal.welch(data_corr[:, i], read_freq, nperseg=256)
            PSDs.append(P1_outputs)
            
        PSDs = np.mean(PSDs, axis=0)
        print (f_vect, P1_outputs)
        figure = noise_plots.display_noisepsd(f_vect, PSDs, 1)
        self.save_fig(figure, 'test1col')
        plt.close()

        """
        # Should be in the simulated data header
        pix_p_output = dimension**2 / nb_output # Number of pixels per output
        nbcols_p_channel = dimension / nb_output # Number of columns per channel
        lperseg = int(pix_p_output / nperseg) # Length of segments for Welch's method

        # Initializing table of nb_outputs periodogram
        Pxx_outputs = np.zeros((nb_output,int(lperseg/2)+1)) # +1 for periodogram dimensions

        # For each output
        for i in tqdm(np.arange(nb_output), ascii=True, ncols=60, desc='PSD Channels'):
            # If i odd, flatten data since its the good reading direction
            if i%2 == 0:
                output_data = data_corr[:,int(i*(nbcols_p_channel)):int((i+1)*(nbcols_p_channel))].flatten()
            # Else, flip it left/right and then flatten it
            else:
                output_data = np.fliplr(data_corr[:,int(i*(nbcols_p_channel)):int((i+1)*(nbcols_p_channel))]).flatten()
            # output data without flipping
            # output_data = data_corr[:,int(i*(nbcols_p_channel)):int((i+1)*(nbcols_p_channel))].flatten()

            ## Add periodogram to the previously initialized array
            f_vect, Pxx_outputs[i] = signal.welch(output_data, read_freq, nperseg=lperseg)

        ## For the detector
#        detector_data = data_corr.flatten()
        ## Add periodogram to the previously initialized array
#        test, Pxx_detector = signal.welch(detector_data, read_freq, nperseg=lperseg)
        if self.module_parameters['channels'] == 'all':
            for idx, psd in enumerate(tqdm((Pxx_outputs), ascii=True, ncols=60, desc='Plot PSDs')):
                figure = noise_plots.display_noisepsd(f_vect, psd,
                                                      'channel '+str('{:02d}'.format(idx)))
                self.save_fig(figure, 'psd-channel'+str(idx))
                plt.close()
        elif self.module_parameters['channels'] == 'mean':
            figure = noise_plots.display_noisepsd(f_vect, np.mean(Pxx_outputs, axis=1))
            self.save_fig(figure, 'psd-averagechannel')
        else:
            sys.exit('Channel keyword not correct')

        figure = noise_plots.display_noisemaps(filename,
                                               **self.module_parameters)
        self.save_fig(figure, 'map')

        self.save_param('test', 42)
        """
        logger.info('Ending noise analysis...')
        # if self.information was updated (for example using self.save_param)
        # returning it will save this info in the output yaml file.
        return self.clean_output()


# MAIN
#########################
if __name__ == "__main__":
    pass


# GARBAGE
#########################
