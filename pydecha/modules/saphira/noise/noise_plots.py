# -*- coding: utf-8 -*-
"""
noise analysis plot module
-------------------------------------------------------------------------------

@author: bserra (ESO)\r
@analysis: gain\r
@creation: Tue May 28 13:46:03 2019

This is a documentation template for the noise analysis plot module.
"""
# IMPORTS
#########################
import os
import sys
from astropy.io import fits
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
from scipy import signal

from pylab import rcParams
rcParams['figure.figsize'] = 15,12
rcParams['font.size'] = 16


# METHODS & CLASSES
#########################
def set_ax(ax):
    """Method to take all subplots in figure, add grid and set both axe to
    same length
    Input: ax object
    Output: None
    """
    import matplotlib.image as im_type

    x_lim, y_lim = ax.get_xlim(), ax.get_ylim()
    if isinstance(ax, im_type.AxesImage):
        ax.grid(True)
    ax.set(adjustable='box',aspect=abs(x_lim[0]-x_lim[1])/abs(y_lim[0]-y_lim[1]))

    return None

def display_noisemaps(filename, **kwargs):
    """Method to display noise distribution from the generated FITS file
    """
    file_hdus = fits.open(filename)
    # noise_infos = get_noise_params(file_hdus[0].header)
    noise_infos = "None"
    data = file_hdus[0].data[0].astype(np.float32)
    print (np.shape(data))
    ## Noise image and distribution
    ##################################
    from mpl_toolkits.axes_grid1 import make_axes_locatable
    from scipy.stats import norm
    import matplotlib.mlab as mlab

    fig, axes = plt.subplots(1,2)
    ## Plotting
    vmin, vmax = np.percentile(data, 0.1), np.percentile(data, 99)
    im = axes[0].imshow(data,vmin=vmin,vmax=vmax)
    n, bins, patches = axes[1].hist(data.flatten(),bins=np.arange(vmin, vmax, (vmax-vmin)/50),label=noise_infos,normed=True);
    # best fit of data
    data = data[(data>vmin)&(data<vmax)]
    (mu, sigma) = norm.fit(data.flatten())
    # add a 'best fit' line (not working in python 3.6 normpdf not there anymore)
    #y = mlab.normpdf( bins, mu, sigma)
    #axes[1].plot(bins, y, 'r--', linewidth=2,label='Gaussian fit:\n$\mu$='+str('{:1.2f}'.format(mu))+'\n$\sigma$='+str('{:1.2f}'.format(sigma)))

    ## Formatting
    axes[1].set_xlabel('Noise [ADU]')
    axes[1].set_ylabel('Frequencies [No unit]')
    [set_ax(ax) for ax in axes]
    axes[1].legend(fontsize=10);
    axes[1].yaxis.set_label_position("right")
    axes[1].yaxis.tick_right()

    # create an axes on the right side of ax. The width of cax will be 5%
    # of ax and the padding between cax and ax will be fixed at 0.05 inch.
    divider = make_axes_locatable(axes[0])
    cax = divider.append_axes("right", size="5%", pad=0.05)
    plt.colorbar(im, cax=cax)
    cax.set_ylabel('Noise [ADU]');
    plt.subplots_adjust(left=0.1,
                        bottom=0.2,
                        right=0.9,
                        top=0.95,
                        hspace=0.2)
    ## Closing file
    file_hdus.close()

    return fig

def display_noisepsd(frequencies, Pxx_output, channel_nb, **kwargs):
    """Method to display noise PSD from the generated FITS file
    :param: kwargs['nb_output'] (int) - Number of outputs of the detector
    :param: kwargs['dimension'] (int) - dimension of the detector (assuming square shape)
    :param: kwargs['read_freq'] (int) - frame rate [Hz]
    :param: kwargs['nperseg'] (int) - Number of segments for Welch's Periodogram
    """
    def set_ax_psd(ax):
        #ax.set_xlim([1, 1e5])
        #ax.set_ylim([1e-6, 1e1])
        ax.set_xscale('log')
        ax.set_yscale('log')
        #if idx != 0:
        ax.set_xlabel('Frequency [Hz]')
        ax.set_ylabel('PSD [${e-}^2/Hz$]')
#         ax.set_ylabel('PSD [ADU${}^2$/Hz]')
        ax.legend(fontsize=12)
        ax.grid(True,alpha=.4)
        plt.subplots_adjust(left=0.1,
                            bottom=0.2,
                            right=0.9,
                            top=0.95,
                            wspace=0.0,
                            hspace=0.2)

    # Plotting
#    plt.suptitle('Noise Power Spectral Density [PSD]\n\
#    Welch seg. length / Nb pixel output: '+str('{:1.2f}'.format(nperseg/pix_p_output))+'\n\
#    File: '+os.path.basename(filename))
    fig, ax = plt.subplots(1,1)
    ax.plot(frequencies, Pxx_output, '-',
            ms=3, alpha=0.3, zorder=32,
            label='PSD output '+str(channel_nb))
#    ax.axvline(x=read_freq/2.,
#               label='Nyquist frequency='+str(read_freq/2)+' Hz', color='red', alpha=0.4)
#    ax.axvline(x=read_freq/(dimension/nb_output),
#               label='Output line end', alpha=0.4)
#    ax.axvline(x=read_freq/dimension,
#               label='Detector line end', color="orange", alpha=0.4)
    set_ax_psd(ax)

    return fig

# MAIN
#########################
if __name__ == "__main__":
    pass


# GARBAGE
#########################
