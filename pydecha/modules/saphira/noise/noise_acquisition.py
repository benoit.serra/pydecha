# -*- coding: utf-8 -*-
"""
noise analysis module
-------------------------------------------------------------------------------

@author: bserra (ESO)\n
@analysis: noise\n
@creation: Mon Jul 15 13:52:52 2019

This is a documentation template for the noise analysis module.
Documentation is generated going into docs/ and typing 'make html'

This module can be found in pydecha/modules/noise/noise_analysis.py.
Please modify the docstrings accordingly to provide the users a simple and
detailed explanation of your algorithm for this module.

Table examples
==============

===============  ==============================================================
Table entries    Table values
===============  ==============================================================
Entry 1          Value 1
Entry 2          Value 2
Entry 3          Value 3

Entry 4          Value 4

Entry 5          Value 5
===============  ==============================================================

+------------------------+------------+----------+----------+
| Header row, column 1   | Header 2   | Header 3 | Header 4 |
| (header rows optional) |            |          |          |
+========================+============+==========+==========+
| body row 1, column 1   | column 2   | column 3 | column 4 |
+------------------------+------------+----------+----------+
| body row 2             | Cells may span columns.          |
+------------------------+------------+---------------------+
| body row 3             | Cells may  | - Table cells       |
+------------------------+ span rows. | - contain           |
| body row 4             |            | - body elements.    |
+------------------------+------------+----------+----------+
| body row 5             | Cells may also be     |          |
|                        | empty: ``-->``        |          |
+------------------------+-----------------------+----------+

Code example
============

.. code-block:: python

    import sys

    print('Hello world...')


.. literalinclude:: ../../../pydecha/modules/cmos/noise/noise_analysis.py
    :language: python
    :linenos:
    :lines: 84-87

Useful links
============

ReadTheDocs documentation
https://sphinx-rtd-theme.readthedocs.io/en/latest/index.html
"""
# IMPORTS
#########################
import logging


# METHODS & CLASSES
#########################
# Function to set DC bias to given value
def set_DC(connection, dc_id, dc_value)
    connection._send_command("ngcbCmd setup DET.CLDC1.DC"+dc_id+" "+dc_value)

# Function to set the filename of the FITS
def set_filename(connection, filename)
    connection._send_command("ngcbCmd setup DET.FRAM.FILENAME "+filename)

# Function to set a parameter
def set_parameter(connection, parameter, value)
    connection._send_command("ngcbCmd setup "+parameter+" "+value)

# Function to load the given configuration
def setup_configuration(connection, configuration:dict)
    #setup NGC with user defined input parameters
    for parameter, value in configuration:
        connection._send_command("set_parameter "+parameter+" "+value)
#    set_parameter DET.READ.CURNAME $program
#    set_parameter DET.SEQ1.DIT $DIT
#    set_parameter DET.NDITSKIP $NDITSKIP
#    set_parameter DET.NDIT $NDIT
#    set_parameter DET.FRAM.FORMAT cube
#    set_parameter DET.ACQ1.QUEUE $queue
#    set_parameter DET.FRAM.FILENAME $filename

#      echo "NGC configuration parameters :"
#      echo "------------------------------"
#      echo "NDIT 		= "$NDIT
#      echo "NDITSKIP 	= "$NDITSKIP
#      echo "DIT 		= "$DIT
#      echo "Delay		= "$DITDELAY
#      echo "Filename 	= "$filename
#      echo "Format 		= "$format
#      echo "Queue 		= "$queue
#      echo "Read-mode 	= "$program
#      echo "NGC Bias/Clock/Program files"
#      echo "----------------------------"
#      echo "Bias file: "$(ngcbCmd status DET.CLDC1.FILE)
#      echo "Clock file: "$(ngcbCmd status DET.SEQ.CLKFILE)
#      echo "Program file: "$(ngcbCmd status DET.SEQ.PRGFILE)

def check_status():
#    test_result="DET.EXP.STATUS \"integrating\""
#    i="DET.EXP.STATUS \"success\""
#    while [ "$test_result" != "$i" ]
#    do
#    echo $test_result
#    sleep 1
#    test_result="$( ngcbCmd status DET.EXP.STATUS )"
#    done
    return None

# Function to start the exposure
def start()
#    echo "Take an exposure"
#    ngcbCmd start
#    check_status
#    sleep 2
    return None

class acquisition(object):
    """Example class with documented types

    :param setting_test_classes (dict): Input parameters for the analysis.

    :return None: The return value. Nothing for now...
    """
    def __init__(self, settings_noise, settings_general):
        self.logger = logging.getLogger('noise_analysis')
        settings_noise.update({'savepath': settings_general['savepath'],
                                   'tstart': settings_general['tstart']})
        self.information = settings_noise
        self.parameters = settings_noise['module_params']

    def start(self):
        """"""
        self.logger.info('Starting noise analysis...')
        # noise code goes here
        print('noise analysis module')
        self.logger.info('Ending noise analysis...')
        return None


# MAIN
#########################
if __name__ == "__main__":
    pass


# GARBAGE
#########################
