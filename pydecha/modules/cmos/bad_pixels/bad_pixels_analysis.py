# -*- coding: utf-8 -*-
"""
bad_pixels analysis module
-------------------------------------------------------------------------------

:author: bserra (ESO)\n
:analysis: bad_pixels\n
:creation: Thu Jul  4 10:55:26 2019

This is a documentation template for the bad_pixels analysis module.
Documentation is generated going into docs/ and typing 'make html'

This module can be found in pydecha/modules/bad_pixels/bad_pixels_analysis.py.
Please modify the docstrings accordingly to provide the users a simple and
detailed explanation of your algorithm for this module.

Table examples
==============

===============  ==============================================================
Table entries    Table values
===============  ==============================================================
Entry 1          Value 1
Entry 2          Value 2
Entry 3          Value 3

Entry 4          Value 4

Entry 5          Value 5
===============  ==============================================================

+------------------------+------------+----------+----------+
| Header row, column 1   | Header 2   | Header 3 | Header 4 |
| (header rows optional) |            |          |          |
+========================+============+==========+==========+
| body row 1, column 1   | column 2   | column 3 | column 4 |
+------------------------+------------+----------+----------+
| body row 2             | Cells may span columns.          |
+------------------------+------------+---------------------+
| body row 3             | Cells may  | - Table cells       |
+------------------------+ span rows. | - contain           |
| body row 4             |            | - body elements.    |
+------------------------+------------+----------+----------+
| body row 5             | Cells may also be     |          |
|                        | empty: ``-->``        |          |
+------------------------+-----------------------+----------+

Code example
============

.. code-block:: python

    import sys

    print('Hello world...')


.. literalinclude:: ../../../pydecha/modules/cmos/bad_pixels/bad_pixels_analysis.py
    :language: python
    :linenos:
    :lines: 84-87

Useful links
============

ReadTheDocs documentation
https://sphinx-rtd-theme.readthedocs.io/en/latest/index.html
"""
# IMPORTS
#########################
# PYDECHA specific methods and classes
import methods.plot_methods as plotm
from classes.analysis_module import analysis


# METHODS & CLASSES
#########################
class module(analysis):
    """Example class with documented types

    :param setting_test_classes (dict): Input parameters for the analysis.

    :return None: The return value. Nothing for now...
    """
    def start(self):
        """"""
        # logger is the logger initialized in analysis_module.analysis
        # depending of the severity of the error the user can use:
        # logger.info
        # logger.warning
        # logger.error
        # logger.critical
        logger = self.logger
        logger.info('Starting bad_pixels analysis...')
        # bad_pixels code goes here
        print('bad_pixels analysis module')
        print('bad_pixels parameters')
        # self.information is a dictionnary containing the module information
        # that were written in the .yaml configuration file
        print(self.information)
        # saving a parameter can be done using self.save_param
        self.save_param('bad_pixels variable name', 'Variable value')

        logger.info('Ending bad_pixels analysis...')
        # if self.information was updated (for example using self.save_param)
        # returning it will save this info in the output yaml file.
        return self.clean_output()


# MAIN
#########################
if __name__ == "__main__":
    pass


# GARBAGE
#########################
