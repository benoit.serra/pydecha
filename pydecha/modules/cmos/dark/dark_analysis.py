# -*- coding: utf-8 -*-
"""
dark analysis module
-------------------------------------------------------------------------------
@author: bserra (ESO)\r
@analysis: dark\r
@creation: Tue May 28 13:46:03 2019
"""
# IMPORTS
#########################
import logging
from methods.plot_methods import show_map
from astropy.io import fits


# METHODS
#########################
def start(settings_dark, settings_general):
    """Example function with types documented in the docstring.

    :param settings_dark (dict): Input parameters for the analysis.

    :return None: The return value. Nothing for now...
    """

    logger = logging.getLogger('dark_analysis')
    logger.info('Starting dark analysis...')
    # dark code goes here
    data_path = settings_dark['path']
    contents = fits.open(data_path)
    datatoanalyse = contents[0].data
    show_map(datatoanalyse,
             min_perc=5,
             max_perc=95,
             title='Dark current',
             **settings_general)
    contents.close()

    logger.info('Ending dark analysis...')

    return None


# MAIN
#########################
if __name__ == "__main__":
    pass


# GARBAGE
#########################
