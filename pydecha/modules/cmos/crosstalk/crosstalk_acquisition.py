# -*- coding: utf-8 -*-
"""
crosstalk acquisition module
-------------------------------------------------------------------------------

+--------------+----------------------------------+---------------------------+
| Author       | Name                             | Creation                  |
+--------------+----------------------------------+---------------------------+
| Benoit Serra | crosstalk                        | %(date)                   |
+--------------+----------------------------------+---------------------------+

+-----------------+-------------------------------------+---------------------+
| Contributor     | Name                                | Creation            |
+-----------------+-------------------------------------+---------------------+
| Name            | filename                            | 06/21/2019          |
+-----------------+-------------------------------------+---------------------+

This is a documentation template for the crosstalk acquisition module.
Documentation is generated going into docs/ and typing 'make html'

This module can be found in pydecha/modules/crosstalk/crosstalk_acquisition.py.
Please modify the docstrings accordingly to provide the users a simple and
detailed explanation of your algorithm for this module.

Table examples
==============

===============  ==============================================================
Table entries    Table values
===============  ==============================================================
Entry 1          Value 1
Entry 2          Value 2
Entry 3          Value 3

Entry 4          Value 4

Entry 5          Value 5
===============  ==============================================================

+------------------------+------------+----------+----------+
| Header row, column 1   | Header 2   | Header 3 | Header 4 |
| (header rows optional) |            |          |          |
+========================+============+==========+==========+
| body row 1, column 1   | column 2   | column 3 | column 4 |
+------------------------+------------+----------+----------+
| body row 2             | Cells may span columns.          |
+------------------------+------------+---------------------+
| body row 3             | Cells may  | - Table cells       |
+------------------------+ span rows. | - contain           |
| body row 4             |            | - body elements.    |
+------------------------+------------+----------+----------+
| body row 5             | Cells may also be     |          |
|                        | empty: ``-->``        |          |
+------------------------+-----------------------+----------+

Code example
============

.. code-block:: python

    import sys

    print('Hello world...')


.. literalinclude:: ../../../pydecha/modules/cmos/crosstalk/crosstalk_acquisition.py
    :language: python
    :linenos:
    :lines: 84-87

Useful links
============

ReadTheDocs documentation
https://sphinx-rtd-theme.readthedocs.io/en/latest/index.html

.. todo::

   Write the documentation for crosstalk
"""
# IMPORTS
#########################
import logging
from classes.acquisition_module import acquisition


# METHODS & CLASSES
#########################
class module(acquisition):
    """Example class with documented types

    :param None: analysis module initialize with the parameters from the yaml conf. file

    :return None: The return value. Nothing for now...
    """
    def start(self):
        """"""
        # logger is the logger initialized in analysis_module.analysis
        # depending of the severity of the error the user can use:
        # logger.info/warning/error/critical
        logger = self.logger
        logger.info('Starting crosstalk acquisition...')
        # noisetest code goes here
        # self.module_parameters is a dictionnary containing the module information
        # that were written in the .yaml configuration file
        logger.info(self.module_parameters)
        
        # Connect to the LLCU
        self.LLCU_cli = self._init_session(self.module_parameters['host'], self.module_parameters['user'])

        # Instrumentation script
        commands = ['TC001|*IDN?']
        self.setup_instrument(commands)
        
        # Acquisition script
        date = self.datenow()
        script = ["ngcbCmd setup DET.FRAM.FILENAME /diska/data/"+date+"data",
                  "ngcbCmd setup DET.SEQ1.DIT 10",
                  "wait 10",
                  "ngcbCmd start"]
        self.setup_ngc(script)

        # Instrumentation script
        commands = ['TC002|*IDN?']
        self.setup_instrument(commands)

        logger.info('Ending crosstalk acquisition...')
        # if self.information was updated (for example using self.save_param)
        # returning it will save this info in the output yaml file.
        return self.clean_output()


# MAIN
#########################
if __name__ == "__main__":
    pass


# GARBAGE
#########################
