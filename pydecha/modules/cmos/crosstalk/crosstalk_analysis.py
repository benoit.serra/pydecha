# -*- coding: utf-8 -*-
"""
crosstalk analysis module
-------------------------------------------------------------------------------

+--------------+-------------+---------------------------+
| Author       | Name        | Creation                  |
+--------------+-------------+---------------------------+
| Benoit Serra | crosstalk   | Thu Jun 27 14:49:14 2019  |
+--------------+-------------+---------------------------+

This module can be found in pydecha/modules/crosstalk/crosstalk_analysis.py.
Please modify the docstrings accordingly to provide the users a simple and
detailed explanation of your algorithm for this module.

.. todo::

  Write the analysis code as well as the documentation
"""
# IMPORTS
#########################
# PYDECHA specific methods and classes
import methods.plot_methods as plotm
from classes.analysis_module import analysis


# METHODS & CLASSES
#########################
class module(analysis):
    """Example class with documented types

    :param setting_test_classes (dict): Input parameters for the analysis.

    :return None: The return value. Nothing for now...
    """
    def start(self):
        """"""
        # logger is the logger initialized in analysis_module.analysis
        # depending of the severity of the error the user can use:
        # logger.info
        # logger.warning
        # logger.error
        # logger.critical
        logger = self.logger
        logger.info('Starting crosstalk analysis...')
        # crosstalk code goes here
        print('crosstalk analysis module')
        print('crosstalk parameters')
        # self.information is a dictionnary containing the module information
        # that were written in the .yaml configuration file
        print(self.module_parameters)
        # saving a parameter can be done using self.save_param
        self.save_param('crosstalk variable name', 'Variable value')

        logger.info('Ending crosstalk analysis...')
        # if self.information was updated (for example using self.save_param)
        # returning self.clean_output() will save this info in the output yaml file.
        return self.clean_output()


# MAIN
#########################
if __name__ == "__main__":
    pass


# GARBAGE
#########################
