# -*- coding: utf-8 -*-
"""
warm_tests acquisition module
-------------------------------------------------------------------------------

+--------------+----------------------------------+---------------------------+
| Author       | Name                             | Creation                  |
+--------------+----------------------------------+---------------------------+
| Benoit Serra | warm_tests                        | Wed Feb 22 10:53:18 2023                   |
+--------------+----------------------------------+---------------------------+

+-----------------+-------------------------------------+---------------------+
| Contributor     | Name                                | Creation            |
+-----------------+-------------------------------------+---------------------+
| Name            | filename                            | 06/21/2019          |
+-----------------+-------------------------------------+---------------------+

This is a documentation template for the warm_tests acquisition module.
Documentation is generated going into docs/ and typing 'make html'

This module can be found in pydecha/modules/warm_tests/warm_tests_acquisition.py.
Please modify the docstrings accordingly to provide the users a simple and
detailed explanation of your algorithm for this module.

Table examples
==============

===============  ==============================================================
Table entries    Table values
===============  ==============================================================
Entry 1          Value 1
Entry 2          Value 2
Entry 3          Value 3

Entry 4          Value 4

Entry 5          Value 5
===============  ==============================================================

+------------------------+------------+----------+----------+
| Header row, column 1   | Header 2   | Header 3 | Header 4 |
| (header rows optional) |            |          |          |
+========================+============+==========+==========+
| body row 1, column 1   | column 2   | column 3 | column 4 |
+------------------------+------------+----------+----------+
| body row 2             | Cells may span columns.          |
+------------------------+------------+---------------------+
| body row 3             | Cells may  | - Table cells       |
+------------------------+ span rows. | - contain           |
| body row 4             |            | - body elements.    |
+------------------------+------------+----------+----------+
| body row 5             | Cells may also be     |          |
|                        | empty: ``-->``        |          |
+------------------------+-----------------------+----------+

Code example
============

.. code-block:: python

    import sys

    print('Hello world...')


.. literalinclude:: ../../../pydecha/modules/cmos/warm_tests/warm_tests_acquisition.py
    :language: python
    :linenos:
    :lines: 84-87

Useful links
============

ReadTheDocs documentation
https://sphinx-rtd-theme.readthedocs.io/en/latest/index.html

.. todo::

   Write the documentation for warm_tests
"""
# IMPORTS
#########################
import logging
from classes.acquisition_module import acquisition

# METHODS & CLASSES
#########################
class module(acquisition):
    """Example class with documented types

    :param None: analysis module initialize with the parameters from the yaml conf. file

    :return None: The return value. Nothing for now...
    """
    def start(self):
        """"""
        # logger is the logger initialized in analysis_module.analysis
        # depending of the severity of the error the user can use:
        # logger.info/warning/error/critical
        logger = self.logger
        logger.info('Starting warm_tests acquisition...')
        # noisetest code goes here
        # self.module_parameters is a dictionnary containing the module information
        # that were written in the .yaml configuration file
        logger.info(self.module_parameters)
        
        # Connect to the LLCU
        self.LLCU_cli = self._init_session(self.module_parameters['host'], self.module_parameters['user'])

        # Instrumentation script
        """
	No communication with instrumentation needed
	"""
 
        # Acquisition script
        acq_name = "warmtest"
        prefix = "METIS-ENG-18852"
        read_modes = ["UnCor",
                      "DblCor"]
        """
        frame_settings=["ngcbCmd frame 0 DIT store 0",
                        "ngcbCmd frame 0 DIT store 1"]
        self.setup_ngc(frame_settings)
        """
        for read_mode in read_modes:
            script = ["ngcbCmd seq 0 stop",
                      f"ngcbCmd setup DET.READ.CURNAME '{read_mode}'",
                      "wait 10",
                      "ngcbCmd setup DET.FRAM.FORMAT cube",
                      "ngcbCmd setup DET.NDIT 20",
                      "ngcbCmd frame 0 DIT store 1",
                      "ngcbCmd frame 0 STDEV gen 1",
                      "ngcbCmd frame 0 STDEV store 1",
                      "ngcbCmd seq 0 start"]
            self.setup_ngc(script)

            for integration_time in ['0']:
                date = self.datenow()
                script = [f"ngcbCmd setup DET.FRAM.FILENAME /diska/data/{date}{prefix}_{acq_name}-DIT{integration_time}",
                          f"ngcbCmd setup DET.SEQ1.DIT {integration_time}",
                          "wait 2",
                          "ngcbCmd start"]
                self.setup_ngc(script)

        logger.info('Ending warm_tests acquisition...')
        # if self.information was updated (for example using self.save_param)
        # returning it will save this info in the output yaml file.
        return self.clean_output()


# MAIN
#########################
if __name__ == "__main__":
    pass


# GARBAGE
#########################
