# -*- coding: utf-8 -*-
"""
gain analysis module
-------------------------------------------------------------------------------

@author: bserra (ESO)\r
@analysis: gain\r
@creation: Tue May 28 13:46:03 2019

This is a documentation template for the gain analysis module.

This module can be found in pydecha/modules/gain/gain_analysis.py.

Conversion gain
===============

The conversion gain of the detector represents the conversion rate between
number of photogenerated charges in the pixel and the Digital Number.

Photon Transfer Curve
=====================

The Photon Transfer Curve (PTC) is the relation between the value and the
variance of the signal. In order to measure it there are several ways:
- Keeping the LED on and take images at different DIT
- Keeping the DIT constant and taking several images with increasing the LED current
Py plotting the signal against the variance, the slope of this curve gives the
reciprocal of the Gain.

Data acquisition
================

The process used was
For the same exposure time, LED values were increased. I can't find any data
on the LED in the FITS headers. For each LED value, a 1.38s CDS was taken. The
process was repeated 102 times, creating 102 FITS files.

Analysis
========

The analysis is a bit different than the typical PTC curve. For all 102 files,
we do the analysis by pair.
We take the both CDS images and subtract them, which gives the signal difference
between two LED values
The variance of the subtraction between two CDS is then divided by two.
The average of the two CDS gives an estimation of the signal that we should have
between the two LED values.

Plotting variance versus average gives the PTC.
"""
# IMPORTS
#########################
import logging
from astropy.io import fits
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import methods.plot_methods as plotm
from tqdm import tqdm


# METHODS & CLASSES
#########################
def plot_ptcboxes(data, **kwargs):
    """
    """
    fig, axes = plt.subplots(2, 1, figsize=(20, 10))
    im1 = axes[0].imshow(data[0],
                           aspect='equal', origin="lower", cmap='gist_heat')
    coord=kwargs['subarray']
    box=kwargs
    plotm.colorbar(im1, scale=1)

    # axes[0].set_title("CDS image at {:.3f} seconds".format(signal1_header["EXPTIME"]))

    for idx_c,c in enumerate(range(coord['start_x'], coord['start_x']+coord['width_x'], box['box_x'])):

        for idx_r,r in enumerate(range(coord['start_y'], coord['start_y']+coord['width_y'], box['box_y'])):

            rect = patches.Rectangle((c,r) ,
                                     box['box_x'], box['box_y'],
                                     linewidth=1, edgecolor='yellow', facecolor='none')
            axes[0].add_patch(rect)

    im2 = axes[1].imshow(data[1], aspect='equal', origin="lower", cmap='gist_heat')
#    plt.colorbar(image, ax=axes[1])
    plotm.colorbar(im2, scale=1)
    #cb.ax.set_ylabel('Dark current [e-/s]')
    # axes[1].set_title("CDS image at {:.3f}".format(signal2_header["EXPTIME"]))

    for idx_c,c in enumerate(range(coord['start_x'], coord['start_x']+coord['width_x'], box['box_x'])):

        for idx_r,r in enumerate(range(coord['start_y'], coord['start_y']+coord['width_y'], box['box_y'])):

            rect = patches.Rectangle((c,r) ,
                                     box['box_x'], box['box_y'],
                                     linewidth=1, edgecolor='yellow', facecolor='none')
            axes[1].add_patch(rect)

    plt.subplots_adjust(left=0.05,
                        bottom=0.2,
                        right=0.95,
                        top=0.95,
                        wspace=0.0,
                        hspace=0.1)

    fig.tight_layout(rect=[0, 0.03, 1, 0.925])
    return fig


def plot_ptc(results, **kwargs):
    """
    """
    fig, axes = plt.subplots(2, 1, figsize=(20, 15))
    # Plot gain vs average signal in boxed pixels
    axes[0].plot(np.array(results['mean']).flatten(),
                 np.array(results['gain']).flatten(),
                 '.', ms=1)
    axes[0].plot(results['med_filt'],
                 results['gain_filt'],
                 '-', color='black')
    # Plot PTC (variance vs average)
    axes[1].plot(np.array(results['mean']).flatten(),
                 np.array(results['variance']).flatten(),
                 '.', ms=1)
    axes[1].plot(results['med_filt'],
                 results['variance_filt'],
                 '-', color='black')

    plotm.set_ax(axes[0], kwargs)

    # scales
    [ax.semilogx() for ax in axes]
    axes[1].set_xscale("log", nonposx='clip')
    axes[1].set_yscale("log", nonposy='clip')
    # Limits and cosmetics
    [ax.set_xlim(100, 100000) for ax in axes]
    axes[0].set_ylim(0, 10)
    [ax.grid(True) for ax in axes]

    plt.subplots_adjust(left=0.1,
                        bottom=0.2,
                        right=0.9,
                        top=0.95,
                        wspace=0.0,
                        hspace=0.2)
    return fig


def plot_gain(results, **kwargs):
    """"""
    fig, ax = plt.subplots(1, 1, figsize=(20, 15))
    ax.plot(results['med_filt'], results['variance_filt'], 'bo')
    if 'fit' in kwargs.keys():
        fit_par = kwargs['fit']
        fit_lim = slice(fit_par['fit_start'], fit_par['fit_stop'])
        p = np.polyfit(results['med_filt'][fit_lim],
                       results['variance_filt'][fit_lim], 1)
        fit = np.poly1d(p)
        ax.plot(results['med_filt'][fit_lim], fit(results['med_filt'][fit_lim]), '-k',
                   label=str('{:1.3f}'.format(1./p[0]))+r' $ADU/e-$')
        gain = '{:1.3f}'.format(1./p[0])
        # Error in respect to the residuals from the fit
        # residuals = ((fit(results['med_filt'])-results['variance_filt'])/fit(results['med_filt']))*100
        # ptop = np.max(residuals)-np.min(residuals)
        # ax[1].plot(results['med_filt'], residuals, 'bo',
        #            label='Residuals' +
        #            str('{:1.3f}'.format(ptop))+'% non linearity')
        plotm.set_ax(ax, kwargs)
    # ax[1].set_ylabel('Residuals')
    # ax[1].grid(True)
    [axe.legend() for axe in fig.get_axes()]
    plt.subplots_adjust(left=0.1,
                        bottom=0.2,
                        right=0.9,
                        top=0.95,
                        wspace=0.0,
                        hspace=0.2)

    return fig, gain



# MAIN
#########################
if __name__ == "__main__":
    pass


# GARBAGE
#########################
