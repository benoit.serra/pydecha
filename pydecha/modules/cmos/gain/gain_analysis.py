# -*- coding: utf-8 -*-
"""
gain analysis module
-------------------------------------------------------------------------------

+-----------------+-------------------------------+---------------------------+
| Author          | Name                          | Creation                  |
+-----------------+-------------------------------+---------------------------+
| Benoit Serra    | Photon Transfer Curve         | Tue May 28 13:46:03 2019  |
+-----------------+-------------------------------+---------------------------+

+-----------------+-------------------------------------+---------------------+
| Contributor     | Name                                | Creation            |
+-----------------+-------------------------------------+---------------------+
| Domingo Alvarez | H4RG PTC Analysis.ipynb             | 06/21/2019          |
+-----------------+-------------------------------------+---------------------+


This module can be found in pydecha/modules/gain/gain_analysis.py.
Please modify the docstrings accordingly to provide the users a simple and
detailed explanation of your algorithm for this module.

Conversion gain
===============

The conversion gain of the detector represents the conversion rate between
number of photogenerated charges in the pixel and the number of photo-generated
carriers.

Photon Transfer Curve
=====================

The Photon Transfer Curve (PTC) is the relation between the value and the
variance of the signal. In order to measure it there are several ways:
- Keeping the LED on and take images at different DIT
- Keeping the DIT constant and taking several images with increasing the LED current
Py plotting the signal against the variance, the slope of this curve gives the
reciprocal of the Gain.

Data acquisition
================

For the same exposure time, LED values were increased. I can't find any data
on the LED in the FITS headers. For each LED value, a 1.38s CDS was taken. The
process was repeated 102 times, creating 102 FITS files.

Computation
===========

The analysis is a bit different than the typical PTC curve. For all 102 files,
we do the analysis by pair. We take both CDS images and subtract them, which
gives the signal difference between two LED values.

The variance of the subtraction between two CDS is then divided by two.
The average of the two CDS gives an estimation of the signal that we should have
between the two LED values.

Since we need a lot of samples for statistical significance of the results,
an average of several pixels is done by defining 'boxes' of pixels where the
spatial variance and average signal is taken to compute one point of the PTC.

Plots
=====

Photon Transfer Curver
######################

Plotting variance versus average gives the PTC for all the boxes. On top of it,
plotting the line going through the median value of all boxes.

Boxes
#####

Representing the location of the boxes in the image.

Gain fit
########

By doing a linear fit of the median values for variance = f(signal), we can
compute the gain by extracting the slope of the fit and taking the reciprocal.

Analysis block
==============
"""
# IMPORTS
#########################
import logging
from astropy.io import fits
import glob
import numpy as np
import matplotlib.pyplot as plt
import methods.pydecha_methods as pdcm
import methods.plot_methods as plotm
import modules.cmos.gain.gain_plots as gain_plots
from tqdm import tqdm
from classes.analysis_module import analysis


# METHODS & CLASSES
#########################
class module(analysis):
    """Gain analysis block

    :param module_name (str): Name of the module as string.
    :param yaml_dict (dict): full dictionnary of the config file.

    :return clean_output(): a cleaned dictionnary with the parameters saved

    The gain analysis requires the following parameters in the configuration
    file:

    :param data_path (str): absolute path of where the data is.
    :param subarray (dict): coordinates of the subarray where the PTC will be
                            computed
    :param rn_subarray (dict): coordinates of the subarray where the noise will
                               be estimated
    :param box_x (int): number of columns for a box
    :param box_y (int): number of rows for a box
    :param fit (dict): fit parameters [fit_start (int), fit_stop (int)]
    """
    def start(self):
        """"""
        self.logger.info('Starting gain analysis...')
        print('Gain analysis module')
        # gain code goes here
        data_path = self.module_parameters['data_path']
        ptc_files = glob.glob(data_path+'/*.fits')
        self.get_dataset_info(ptc_files,
                              file_type='FITS',
                              save=True)
        # Tiles dimension
        window = self.module_parameters['subarray']
        tile_x = self.module_parameters['box_x']
        tile_y = self.module_parameters['box_y']
        readnoise_window = self.module_parameters['rn_subarray']

        results = {'mean': list(),
                   'gain': list(),
                   'variance': list()}

        # tqdm around an iterator will print a progress bar while the loop runs.
        pbar = tqdm(ptc_files[len(ptc_files)//2:], ascii=True, ncols=60)
        pbar.set_description('Photon Transfer Curve')
        for idx, filename in enumerate(pbar):
            self.logger.info('Loop '+str(idx))
            # Opening files
            file1_data, file1_hdr = fits.getdata(ptc_files[2*idx], 0,
                                                 header=True)
            file2_data, file2_hdr = fits.getdata(ptc_files[2*idx+1], 0,
                                                 header=True)
            self.logger.info(' Files used: '+str(ptc_files[2*idx]))
            self.logger.info('   - '+str(ptc_files[2*idx]))
            self.logger.info('   - '+str(ptc_files[2*idx+1]))
            # If different exposure time for the pair, analysis is useless
            if file1_hdr['EXPTIME'] != file2_hdr['EXPTIME']:
                self.logger.critical('Image pair have a different exposure time')

            # Getting the readnoise (from the reference pixels?)
            readnoise = np.std(file1_data[readnoise_window['start_y']:readnoise_window['end_y'],
                                          readnoise_window['start_x']:readnoise_window['end_x']])
            readnoise = 0
            # Compute the number of tiles in the window
            tiles_inx = (window['width_x']+1)/tile_x
            tiles_iny = (window['width_y']+1)/tile_y
            # Total number of tiles
            # tiles_inwindow = tiles_inx*tiles_iny
            # Compute images (subtraction and mean of the two)
            image_sub = file2_data - file1_data
            image_mean = np.mean(np.array([file2_data, file1_data]), axis=0)
            # Initialize lists
            mean, std, gain, variance = list(), list(), list(), list()
            for y in np.arange(0, tiles_iny):
                for x in np.arange(0, tiles_inx):
                    # define the slices for the box
                    arr_slice_x = slice(int(window['start_x']+x*tile_x),
                                        int(window['start_x']+(x+1)*tile_x-1))
                    arr_slice_y = slice(int(window['start_y']+y*tile_y),
                                        int(window['start_y']+(y+1)*tile_y-1))
                    # Average what?
                    tile_mean = np.nanmean(image_mean[arr_slice_y, arr_slice_x])
                    mean.append(tile_mean)
                    # Standard deviation
                    tile_std = np.nanstd(image_sub[arr_slice_y, arr_slice_x])
                    std.append(tile_std)

                    # Compute gain and variance
                    # divide the variance by two because we are taking CDS images
                    # so the real variance associated to the noise is
                    # var_noise = (noise_cds/np.sqrt(2))**2
                    # tile_mean/((tile_std**2-readnoise**2)/2)

                    # Append results to the list created beforehand
                    gain.append(tile_mean/((tile_std**2-readnoise**2)/2))
                    variance.append(tile_std**2/2)

            results['mean'].append(mean)
            results['gain'].append(gain)
            results['variance'].append(variance)

        results['med_filt'] = [np.nanmedian(mean_array)
                               for mean_array in results['mean']]
        results['gain_filt'] = [np.nanmedian(gain_array)
                                for gain_array in results['gain']]
        results['variance_filt'] = [np.nanmedian(variance_array)
                                    for variance_array in results['variance']]

        pinfos = {'x_label': 'Average signal [ADU]',
                  'y_label': 'Detector gain [e-/ADU]',
                  'title': 'Photon transfer curve with varying exposure times.'}
        self.module_parameters.update(**pinfos)
        fig = gain_plots.plot_ptcboxes([fits.getdata(ptc_files[0]),fits.getdata(ptc_files[-1])],
                                 **self.module_parameters)
        self.save_fig(fig, 'boxes')
        fig = gain_plots.plot_ptc(results, **self.module_parameters)
        self.save_fig(fig, 'ptc')

        fig, gain = gain_plots.plot_gain(results, **self.module_parameters)
        self.save_fig(fig, 'gain-fit')

        self.save_param('gain', gain)
        self.logger.info('Ending gain analysis...')
        return self.clean_output()

# MAIN
#########################
if __name__ == "__main__":
    pass


# GARBAGE
#########################
