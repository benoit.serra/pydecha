# -*- coding: utf-8 -*-
"""
preampgain analysis module
-------------------------------------------------------------------------------

+-----------------+-------------------------------+---------------------------+
| Author          | Name                          | Creation                  |
+-----------------+-------------------------------+---------------------------+
| Benoit Serra    | preamp gain                   | Mon Jun 17 15:29:35 2019  |
+-----------------+-------------------------------+---------------------------+

+-----------------+-------------------------------------+---------------------+
| Contributor     | Name                                | Creation            |
+-----------------+-------------------------------------+---------------------+
| Domingo Alvarez | H4RG-Analysis_PreampGain.ipynb      | 06/21/2019          |
+-----------------+-------------------------------------+---------------------+

This module can be found in pydecha/modules/preampgain/preampgain_analysis.py.
Please modify the docstrings accordingly to provide the users a simple and
detailed explanation of your algorithm for this module.

Computation
===========

All the data extraction is done to a Pandas Dataframe. The extracted data is
the full image mean value as well as the telemetry value of the PreampRef
provided by NGC.

Plots
=====

Linear fit to the data
######################

The average value of the array versus the PreampRef value is then plotted.
Using Numpy and a polynomial fit of first order, the preamp gain and its offset
is obtained using the data extracted to the reduced_data dataframe.

Analysis block
==============
"""
# IMPORTS
#########################
import logging
from astropy.io import fits
import glob
import numpy as np
import matplotlib.pyplot as plt
import methods.pydecha_methods as pdcm
import methods.plot_methods as plotm
from tqdm import tqdm
from classes.analysis_module import analysis


# METHODS & CLASSES
#########################
class module(analysis):
    """Cell preamp gain analysis block

    :param module_name (str): Name of the module as string.
    :param yaml_dict (dict): full dictionnary of the config file.

    :return clean_output(): a cleaned dictionnary with the parameters saved

    The preampgain analysis requires the following parameters in the
    configuration file:

    :param data_path (str): absolute path of where the data is.
    :param fit (dict): fit parameters [fit_start (int), fit_stop (int), fit_unit (str)]
    """
    def start(self):
        """"""
        self.logger.info('Starting preampgain analysis...')
        # noise code goes here
        print('preampgain analysis module')
        data_path = self.module_parameters['data_path']
        preampgain_files = glob.glob(data_path+'/*PreampGain.fits')
        self.get_dataset_info(preampgain_files,
                              file_type='FITS',
                              save=True)
        preamp_ref, detector_signal = list(), list()
        # tqdm around an iterator will print a progress bar while the loop runs.
        pbar = tqdm(preampgain_files, ascii=True, ncols=60)
        pbar.set_description('Preamp Gain Computation')
        for idx, filename in enumerate(pbar):
            self.logger.info('Loop '+str(idx))
            # Opening files
            file_data, file_hdr = fits.getdata(filename, 0,
                                               header=True)
            self.logger.info(' File used: '+str(filename))
            detector_signal.append(np.mean(file_data))
            preamp_ref.append(file_hdr['HIERARCH ESO DET CLDC1 DCT7'])

        pinfos = {'x_label': 'Preamp reference voltage [V]',
                  'y_label': 'Detector signal [ADU]',
                  'title': 'Detector signal evolution versus change in DC7 - PreampRef'}
        self.module_parameters.update(**pinfos)
        fig, ax, preampgain = plotm.fitline_plot(preamp_ref, detector_signal,
                                                 **self.module_parameters)
        self.save_param('PreampGain', preampgain)
        self.save_fig(fig, 'PreampRefvsSignal.png')

        self.logger.info('Ending preampgain analysis...')
        return self.clean_output()


# MAIN
#########################
if __name__ == "__main__":
    pass


# GARBAGE
#########################
