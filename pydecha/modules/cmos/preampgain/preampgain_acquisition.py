# -*- coding: utf-8 -*-
"""
preampgain acquisition module
-------------------------------------------------------------------------------

+--------------+----------------------------------+---------------------------+
| Author       | Name                             | Creation                  |
+--------------+----------------------------------+---------------------------+
| Benoit Serra | preampgain                       | Mon Jun 17 15:29:35 2019  |
+--------------+----------------------------------+---------------------------+

+-----------------+-------------------------------------+---------------------+
| Contributor     | Name                                | Creation            |
+-----------------+-------------------------------------+---------------------+
| Name            | filename                            | 06/21/2019          |
+-----------------+-------------------------------------+---------------------+

Preamp gain
===========

The signal of the pixel (in Volts) pass through the cryogenic preamp board
with a gain different from unity.


Data acquisition
================

To measure the cell gain, the value of VReset for the pixel must be kept constant.
In order to ensure a constant VReset, the RESETEN clock low was set close to
its high value (3.1 V). This value is close enough to the high value of the other
rail, effectively forcing the Pixel Cell to be constantly reseted while reading.

Then the PreampRef voltage value has been sweeped from 1.6 to 2.3V.
DSub and VReset values stay the same.

Acquisition block
=================
"""
# IMPORTS
#########################
import logging
from classes.acquisition_module import acquisition


# METHODS & CLASSES
#########################
class module(acquisition):
    """Example class with documented types

    :param None: analysis module initialize with the parameters from the yaml conf. file

    :return None: The return value. Nothing for now...
    """
    def start(self):
        """"""
        # logger is the logger initialized in analysis_module.analysis
        # depending of the severity of the error the user can use:
        # logger.info/warning/error/critical
        logger = self.logger
        logger.info('Starting preampgain acquisition...')
        # noisetest code goes here
        # self.module_parameters is a dictionnary containing the module information
        # that were written in the .yaml configuration file
        logger.info(self.module_parameters)
        
        # Connect to the LLCU
        self.LLCU_cli = self._init_session(self.module_parameters['host'], self.module_parameters['user'])

        # Instrumentation script
        commands = ['TC001|*IDN?']
        self.setup_instrument(commands)
        
        # Acquisition script
        date = self.datenow()
        script = ["ngcbCmd setup DET.FRAM.FILENAME /diska/data/"+date+"data",
                  "ngcbCmd setup DET.SEQ1.DIT 10",
                  "wait 10",
                  "ngcbCmd start"]
        self.setup_ngc(script)

        # Instrumentation script
        commands = ['TC002|*IDN?']
        self.setup_instrument(commands)

        logger.info('Ending preampgain acquisition...')
        # if self.information was updated (for example using self.save_param)
        # returning it will save this info in the output yaml file.
        return self.clean_output()


# MAIN
#########################
if __name__ == "__main__":
    pass


# GARBAGE
#########################
