# -*- coding: utf-8 -*-
"""
cosmetics analysis module
-------------------------------------------------------------------------------

+-----------------+-------------------------------+---------------------------+
| Author          | Name                          | Creation                  |
+-----------------+-------------------------------+---------------------------+
| Benoit Serra    | cosmetics                     | Thu Jul  4 11:47:56 2019  |
+-----------------+-------------------------------+---------------------------+

+-------------------+-----------------------------------+---------------------+
| Contributor       | Name                              | Creation            |
+-------------------+-----------------------------------+---------------------+
| Elizabeth Georges | H4RG-Analysis_Cosmetics.ipynb     | 07/04/2019          |
+-------------------+-----------------------------------+---------------------+

This module can be found in pydecha/modules/cosmetics/cosmetics_analysis.py.
Please modify the docstrings accordingly to provide the users a simple and
detailed explanation of your algorithm for this module.

Cosmetics
=========

The cosmetics of the detector is an elegant way to say "pixels that don't behaves
properly". This group several types of pixels:

Disconnected/Dead pixels
    Pixels that do not convert photon to photo_generated carriers

Hot/Noisy pixels
    Pixels that always displays a higher than average signal of the pixel

Low QE pixels
    No description yet

Cosmic/RTN pixels
    Pixels that have been hit by cosmics while integrating signal or that
    are subject to Random Telegraphic Noise.

Data acquisition
================

This script exists to analyze the cosmetics data taken using the following
procedure:

Noise data:
    Double correlated: MINDIT, NDIT = 50, store INT and STDEV frame. Take 5 data set.

Connection data:
    Uncorrelated: MINDIT, NDIT = 10, store INT frame. Take 1 frame at 250 mV bias and a second at 300 mV bias (all other parameters the same) (repeat 5x)

Dark current data:
    Double correlated: DIT = 60s, NDIT = 10, store INT and STDEVframe. Take 5 data sets.

Computation
===========

1. Start with a badpix map that is 4096 x 4096 px with a value of 0 everywhere.

2. Flag all disconnected pixels:

  a. Take 250mV bias frame and subtract it from the 300mV bias frame to produce a difference frame.
  b. Make a histogram of the pixel values in the difference frame. Record center and sigma. (Alternatively, use median and stddev of this frame, which will be close enough for this.)
  c. Any pixels in difference frame within 5 sigma of 0 are disconnected. Set to 2 in badpix map (note - need to sanity check this to ensure that the center of the distribution is >10 sigma from zero for this threshold to make sense!)

3. Flag all noisy/RTN pixels:

  a. Make a histogram of the pixel values in the "Noise STDDEV frame" and fit a skewed gaussian. Record center and sigma. (Alternatively, use median and stddev of this frame, which will be close enough for this.)
  b. Any pixel with value in the "Noise STDDEV frame" > 5 sigma from center in all 5 datasets = noisy. Set to 4 in badpix map.
  c. Any pixel with value in the "Noise STDDEV frame" > 5 sigma from center in some but not all datasets = RTN. Set to 8 in badpix map.

4. Flag all hot pixels:

  a. Make a histogram of the pixel values in the "Dark INT frame" and fit a skewed gaussian. Record center and sigma. (Alternatively, use median and stddev of this frame, which will be close enough for this.)
  b. Any pixels with value in all 5 data sets of "DARK INT frame" >5 sigma center = hot. Set to 16 in badpix map. (Note - since we expect ~60e- background, this will only catch REALLY hot pixels, e.g. pixels with > ~100 e- dark current in a 1 minute exposure.)

5. Optional: flag cosmics:

  a. Make a histogram of the pixel values in the "Dark INT frame" and fit a skewed gaussian. Record center and sigma. (Alternatively, use median and stddev of this frame, which will be close enough for this.)
  b. Any pixels with value in some but not all datasets of "DARK INT frame" >5 sigma center = cosmics. Set to 32 in badpix map.

6. Create an overall (binary) bad pixel image for display purposes- display an image with black = badpix, white = goodpix.

Note: values of badpix and their meanings may change later!

Plots
=====

TODO
####

.. todo::

  Need to put proper plots for the cosmetics

Analysis block
==============

"""
# IMPORTS
#########################
# PYDECHA specific methods and classes
import methods.plot_methods as plotm
from classes.analysis_module import analysis
import glob
import numpy as np
import astropy.io.fits as fits
from tqdm import tqdm
import sys


# METHODS & CLASSES
#########################
class module(analysis):
    """Cosmetics analysis block

    :param module_name (str): Name of the module as string.
    :param yaml_dict (dict): full dictionnary of the config file.

    :return clean_output(): a cleaned dictionnary with the parameters saved

    The cosmetics analysis requires the following parameters in the configuration
    file:

    :param data_path (str): absolute path of where the data is.
    :param cutoff (int): cutoff value for the outliers (generally cutoff*std).
    """
    def start(self):
        """"""
        # logger is the logger initialized in analysis_module.analysis
        logger = self.logger
        logger.info('Starting cosmetics analysis...')
        # cosmetics code goes here
        fits_files = glob.glob(self.module_parameters['data_path']+'/*.fits')
        # dataset_info is a pandas dataframe with all .fits file and some
        # of their parameters
        dataset_info = self.get_dataset_info(fits_files,
                                             file_type='FITS',
                                             save=True)
        cutoff_coef = self.module_parameters['cutoff']
        self.cosmetic_metadata = dict()
        # _____________________________________________________________________
        # 1. Disconnected pixels
        logger.info('disconnected pixels')
        #sort dataframe by mode and VRESET to find the high and low bias uncorrelated frames
        bymodeVRESET = dataset_info.groupby(['type', 'Mode', 'VRESET'])
        highbias = bymodeVRESET.get_group(('', 'COLD-H4RG_UnCor_100KHz-32CH', 0.205))
        lowbias = bymodeVRESET.get_group(('', 'COLD-H4RG_UnCor_100KHz-32CH', 0.255))

        data_high = np.zeros((4096, 4096))
        data_low = np.zeros((4096, 4096))
        disconnected_map = np.zeros((4096, 4096))
        if len(highbias.index) == len(lowbias.index):
            iterrable = zip(highbias.iterrows(), lowbias.iterrows())
            pbar = tqdm(iterrable, ascii=True, ncols=60)
            pbar.set_description('Disconnected pixels')
            for idx, (high, low) in enumerate(pbar):
                # Taking [1], each row is a tuple (index, dict(var:value))
                logger.info(high[1]['filename']+'\n'+low[1]['filename'])
                data_high += fits.getdata(high[1]['filename'])
                data_low += fits.getdata(low[1]['filename'])
            data_high /= len(highbias.index)
            data_low /= len(lowbias.index)

            bias_diff = data_high - data_low
            disconnected_coord = np.where(bias_diff > -cutoff_coef)
            disconnected_map[disconnected_coord] = 2

            fig = plotm.show_map(bias_diff,
                                 min_perc=0,
                                 max_perc=99,
                                 title='disconnected',
                                 **self.module_parameters)
            # saving a parameter can be done using self.save_param
            self.cosmetic_metadata['discopix'] = np.shape(disconnected_coord)[1]
            self.save_fig(fig, 'discopix')

        # _____________________________________________________________________
        # 2. Noisy/RTN pixels
        logger.info('noisy/RTN pixels')
        bymodedit = dataset_info.groupby(['type', 'Mode', 'DIT'])
        noise = bymodedit.get_group(('std-dev',
                                     'COLD-H4RG_DblCor_100KHz-32CH',
                                     dataset_info['DIT'].min()))
        data_noise = np.zeros((4096, 4096))
        noisy_map = np.zeros((4096, 4096))
        iterrable = noise.iterrows()
        pbar = tqdm(iterrable, ascii=True, ncols=60)
        pbar.set_description('Noisy pixels')
        for idx, noise_file in enumerate(pbar):
            logger.info(noise_file[1]['filename'])
            frame = fits.getdata(noise_file[1]['filename'])
            noisy_coord = np.where(frame > np.median(frame)+cutoff_coef*np.std(frame))
            noisy_map[noisy_coord] +=1
            data_noise += frame
        data_noise /= len(noise.index)
        noisy_map[np.where(noisy_map == 5)] = 4

        fig = plotm.show_map(data_noise,
                             min_perc=0,
                             max_perc=99,
                             title='noisy',
                             **self.module_parameters)
        # saving a parameter can be done using self.save_param
        self.cosmetic_metadata['noisypix'] = np.shape(noisy_coord)[1]
        self.save_fig(fig, 'noisypix')

        # _____________________________________________________________________
        # 3. Hot pixels
        logger.info('hot pixels')
        bymodedit = dataset_info.groupby(['type', 'Mode', 'DIT'])
        dark = bymodedit.get_group(('',
                                    'COLD-H4RG_DblCor_100KHz-32CH',
                                    60))
        data_dark = np.zeros((4096, 4096))
        hot_map = np.zeros((4096, 4096))
        iterrable = dark.iterrows()
        pbar = tqdm(iterrable, ascii=True, ncols=60)
        pbar.set_description('Hot pixels')
        for idx, dark_file in enumerate(pbar):
            logger.info(dark_file[1]['filename'])
            frame = fits.getdata(dark_file[1]['filename'])
            hot_coord = np.where(frame > 3*np.median(frame))
            hot_map[hot_coord] += 1
            data_dark += frame
        data_dark /= len(dark.index)
        hot_map[np.where((hot_map > 0) & (hot_map < 5))] = 32
        hot_map[np.where(hot_map == 5)] = 16

        fig = plotm.show_map(data_dark,
                             min_perc=0,
                             max_perc=99,
                             title='hot',
                             **self.module_parameters)
        # saving a parameter can be done using self.save_param
        self.cosmetic_metadata['hotpix'] = np.shape(hot_coord)[1]
        self.save_fig(fig, 'hotpix')

        # _____________________________________________________________________
        # 4. Low QE pixels
        logger.info('low qe pixels')
        bymodedit = dataset_info.groupby(['type', 'Mode', 'DIT'])
        flat = bymodedit.get_group(('',
                                    'COLD-H4RG_DblCor_100KHz-32CH',
                                    60))
        data_flat = np.zeros((4096, 4096))
        lowqe_map = np.zeros((4096, 4096))
        iterrable = flat.iterrows()
        pbar = tqdm(iterrable, ascii=True, ncols=60)
        pbar.set_description('Low QE pixels')
        for idx, flat_file in enumerate(pbar):
            logger.info(flat_file[1]['filename'])
            frame = fits.getdata(flat_file[1]['filename'])
            lowqe_coord = np.where(frame < np.median(frame) - 5*np.sqrt(np.median(frame)))
            lowqe_map[lowqe_coord] += 1
            data_flat += frame
        data_flat /= len(flat.index)
        lowqe_map[np.where((lowqe_map > 0) & (lowqe_map < 5))] = 64
        lowqe_map[np.where(lowqe_map == 5)] = 64

        fig = plotm.show_map(data_flat,
                             min_perc=0,
                             max_perc=99,
                             title='lowqe',
                             **self.module_parameters)
        # saving a parameter can be done using self.save_param
        self.cosmetic_metadata['lowqepix'] = np.shape(lowqe_coord)[1]
        self.save_fig(fig, 'lowqe')
        self.save_param('cosmetics', self.cosmetic_metadata)

        data = [disconnected_map,
                noisy_map,
                hot_map,
                lowqe_map]

        self.save_map(np.array(data).astype(np.int16),
                      file_type='FITS',
                      metadata=self.cosmetic_metadata)
        logger.info('Ending cosmetics analysis...')
        # if self.information was updated (for example using self.save_param)
        # returning it will save this info in the output yaml file.
        return self.clean_output()


# MAIN
#########################
if __name__ == "__main__":
    pass


# GARBAGE
#########################
