# -*- coding: utf-8 -*-
"""
monitoring analysis module
-------------------------------------------------------------------------------

+--------------+--------------+---------------------------+
| Author       | Name         | Creation                  |
+--------------+--------------+---------------------------+
| Benoit Serra | monitoring   | Mon Jun 17 15:29:35 2019  |
+--------------+--------------+---------------------------+

This module can be found in pydecha/modules/monitoring/monitoring_analysis.py.
Please modify the docstrings accordingly to provide the users a simple and
detailed explanation of your algorithm for this module.

Data extraction
===============

The monitoring data is a serie of .txt files in the directory that is defined
in the 'params/data_path' of the config file.
Each file should respect the formatting adopted for PYMONT:

Comment lines
  Preceded by '#'

Separators
  Fields are separated by tabs '\\t'

All instruments are defined by their tag, for example, all lakeshores are
usually defined with a 'T' followed by a three digit number.

Conventions can change, but the monitoring module use the conventions of
pymont:

T
  Temperature controller (Lakeshores)

P
  Pressure gauges controller (TPG262)

C
  Compressor (Sumitomo F70)

Plotting the data
=================

The plots are divided by instruments. For now there is no option to mix the
plots from different instruments.
For now only three categories are available

These plotting functions are linked to the instrument tag using a dictionnary:

.. literalinclude:: ../../../../pydecha/modules/cmos/monitoring/monitoring_analysis.py
    :language: python
    :linenos:
    :lines: 269-271

Analysis block
==============
"""
# IMPORTS
#########################
import pandas as pd
import matplotlib.pyplot as plt
import glob
import numpy as np
import methods.pydecha_methods as pdcm
import methods.plot_methods as plotm
from tqdm import tqdm
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
from classes.analysis_module import analysis


# METHODS & CLASSES
#########################
class module(analysis):
    """Monitoring analysis block

    :param module_name (str): Name of the module as string.
    :param yaml_dict (dict): full dictionnary of the config file.

    :return clean_output(): a cleaned dictionnary with the parameters saved

    The monitoring analysis requires the following parameters in the configuration
    file:

    :param data_path (str): absolute path of where the data is.
    :param instruments (list): list of all instrument to display in the plot.
    :param comments (bool): True to allow the comments in monitoring file to be
                            displayed on the graph.
    """
    def start(self):
        """"""
        self.logger.info('Starting test_classes analysis...')
        # test_classes code goes here
        print('Monitoring analysis module')
        # monitoring code goes here
        files = glob.glob(self.module_parameters['data_path']+'/*.txt')
        # Looping over the data files
        for idx, file in enumerate(tqdm(files, ascii=True,
                                        desc='Load files', ncols=60)):
            # Use pandas to read it, seperators are tabs, 'Time' is an object
            # comments are preceded by a #
            txt = pd.read_csv(file, sep='\t',
                              dtype={'Time': object}, header=0, comment='#')
            # Verifying that dates have the correct format
            try:
                pd.to_datetime(txt.Time, format='%Y-%m-%d_%H-%M-%S')
            except:
                self.logger.warning('Time format in file '+file+' is incorrect')
            try:
                for field_name in txt.columns:
                    if field_name not in ['Time', 'Comments']:
                        pd.to_numeric(txt[field_name], errors='raise')
            except:
                txt = txt.iloc[1:]
                self.logger.warning('Float format in file '+file+' is incorrect')

            # If this is the first file create the dataframe
            if idx == 0:
                df = txt
            # Otherwise append to this dataframe
            else:
                df = pd.concat([df, txt], ignore_index=True, sort=True)

        # Convert all the timestamps to datetime objects
        df['Time'] = pd.to_datetime(df.Time, format='%Y-%m-%d_%H-%M-%S')
        # Sort the dataframe using the timestamps
        df = df.sort_values(by=['Time'])
        # Reset indexing once sorting is done
        df = df.reset_index(drop=True)

        # The data are the points where Comments field is empty
        df_data = df[df['Reading'].notna()]
        # The comments are the points where comments is not empty
        df_comments = df[df['Comments'].notna()]
        # Plot
        self.plot_results(df_data, df_comments)
        self.logger.info('Ending monitoring analysis...')
        return self.clean_output()


    def plot_results(self, dataframe, datacomments):
        """"""
        # Plot
        #############################
        # list of the instruments in the config file
        instruments_ids = self.module_parameters['instruments']
        # Cast everyparameter as str for when we want to dump it
        information_str = dict()
        [information_str.update({key: str(value)}) for key, value in self.module_parameters.items()]
        # For each ID of instrument
        for inst_id in tqdm(instruments_ids, ascii=True,
                            desc='Plot data', ncols=60):
            field_present = False
            fig, ax = None, None
            # For each column of the pd dataframe
            for field in dataframe.columns:
                # If the instrument is in thie field
                if field.split('_')[0] == inst_id:
                    # Then the instrument is present in the file
                    field_present = True
                    # Plot using the instrument_plots dictionnary
                    fig, ax = instrument_plots[field.split('_')[0][:-3]](dataframe,
                                                                         field,
                                                                         fig,
                                                                         ax)

            if self.module_parameters['comments'] is True:
                comments_filename = (self.savepath + "/" +
                                     self.module_parameters['general']['tstart'] +
                                     '_comments-monitoring.txt')
                comments_file = open(comments_filename, 'w')
                # Add notes from the comments in the file
                for idx, note in datacomments.iterrows():
                    if 'Error instrument' not in note['Comments']:
                        # write the comment in csv, but omit empty fields
                        comments_file.write(note.dropna().to_csv(header=True, index=False))
                        if type(ax) is np.ndarray:
                            # Create a dashed line at the timestamp of the comments
                            ax[0].axvline(x=note['Time'],ls='--',color='grey')
                            # Add a text which correspond to the index of the comment
                            ax[0].text(note['Time'],290,idx,rotation=45, horizontalalignment='right',verticalalignment='top')
                        else:
                            # Create a dashed line at the timestamp of the comments
                            ax.axvline(x=note['Time'],ls='--',color='grey')
                            # Add a text which correspond to the index of the comment
                            ax.text(note['Time'],290,idx,rotation=45, horizontalalignment='right',verticalalignment='top')
                comments_file.close()

            # If the instrument was in the data, there is a plot
            if field_present is True:
                [ax.grid(True) for ax in fig.get_axes()]
                # Add the legend
                [ax.legend(fontsize=12) for ax in fig.get_axes()]
                # Tag the figure with infos
                self.save_fig(fig, inst_id)
            # Else, the instrument was not in data
            else:
                self.logger.warning('Instrument id '+inst_id+' is not present in data')

        return None


def plot_pressure(dataframe, field, fig=None, ax=None):
    """"""
    if (fig, ax) == (None, None):
        fig, ax = plt.subplots(1, 1, figsize=(30, 20))

    ax.plot(dataframe['Time'].astype('O'),
            dataframe[field],
            ls='-',
            label=field.replace('_', '\_'))
    ax.semilogy()
    ax.set_ylabel('Pressure [mbar]')
    ax.set_xlabel('Date [UTC time]')

    return fig, ax


def plot_lakeshore(dataframe, field, fig=None, ax=None):
    """"""
    if (fig, ax) == (None, None):
        fig, ax = plt.subplots(2, 1, figsize=(30, 20))

    if '_H' not in field:
        ax[0].plot(dataframe['Time'].astype('O'),
                   dataframe[field],
                   ls='-',
                   label=field.replace('_', ' '))
    else:
        ax[1].plot(dataframe['Time'].astype('O'),
                   dataframe[field],
                   ls='-',
                   label=field.replace('_', ' '))
    return fig, ax


def plot_compressor(dataframe, field, fig=None, ax=None):
    """"""
    if (fig, ax) == (None, None):
        fig, ax = plt.subplots(3, 1, figsize=(30, 20))

    if '_temp' in field:
        ax[0].plot(dataframe['Time'].astype('O'),
                   dataframe[field],
                   ls='-',
                   label=field.replace('_', ' '))
    elif '_status' in field:
        ax[2].plot(dataframe['Time'].astype('O'),
                   dataframe[field],
                   ls='-',
                   label=field.replace('_', ' '))
    else:
        ax[1].plot(dataframe['Time'].astype('O'),
                   dataframe[field],
                   ls='-',
                   label=field.replace('_', ' '))

    ax[0].set_ylabel('Temperature [C]')
    ax[1].set_ylabel('Pressure [psig]')
    ax[2].set_ylabel('Status [AU]')

    return fig, ax


instrument_plots = {'T': plot_lakeshore,
                    'P': plot_pressure,
                    'C': plot_compressor}

# MAIN
#########################
if __name__ == "__main__":
    pass


# GARBAGE
#########################
"""
@author: bserra (ESO)\n
@analysis: monitoring\n
@creation: Tue Jun 11 12:23:07 2019
"""
