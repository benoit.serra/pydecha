# -*- coding: utf-8 -*-
"""
test_module analysis module
-------------------------------------------------------------------------------

@author: bserra (ESO)\n
@analysis: test_module\n
@creation: Mon Mar 25 13:08:51 2024

This is a documentation template for the test_module analysis module.
Documentation is generated going into docs/ and typing 'make html'

This module can be found in pydecha/modules/test_module/test_module_analysis.py.
Please modify the docstrings accordingly to provide the users a simple and
detailed explanation of your algorithm for this module.

Table examples
==============

===============  ==============================================================
Table entries    Table values
===============  ==============================================================
Entry 1          Value 1
Entry 2          Value 2
Entry 3          Value 3

Entry 4          Value 4

Entry 5          Value 5
===============  ==============================================================

+------------------------+------------+----------+----------+
| Header row, column 1   | Header 2   | Header 3 | Header 4 |
| (header rows optional) |            |          |          |
+========================+============+==========+==========+
| body row 1, column 1   | column 2   | column 3 | column 4 |
+------------------------+------------+----------+----------+
| body row 2             | Cells may span columns.          |
+------------------------+------------+---------------------+
| body row 3             | Cells may  | - Table cells       |
+------------------------+ span rows. | - contain           |
| body row 4             |            | - body elements.    |
+------------------------+------------+----------+----------+
| body row 5             | Cells may also be     |          |
|                        | empty: ``-->``        |          |
+------------------------+-----------------------+----------+

Code example
============

.. code-block:: python

    import sys

    print('Hello world...')


.. literalinclude:: ../../../pydecha/modules/ccd/test_module/test_module_analysis.py
    :language: python
    :linenos:
    :lines: 84-87

Useful links
============

ReadTheDocs documentation
https://sphinx-rtd-theme.readthedocs.io/en/latest/index.html
"""
# IMPORTS
#########################
import logging


# METHODS & CLASSES
#########################
class analysis(object):
    """Example class with documented types

    :param setting_test_classes (dict): Input parameters for the analysis.

    :return None: The return value. Nothing for now...
    """
    def __init__(self, settings_test_module, settings_general):
        self.logger = logging.getLogger('test_module_analysis')
        settings_test_module.update({'savepath': settings_general['savepath'],
                                   'tstart': settings_general['tstart']})
        self.information = settings_test_module
        self.parameters = settings_test_module['module_params']

    def start(self):
        """"""
        self.logger.info('Starting test_module analysis...')
        # noise code goes here
        print('test_module analysis module')
        self.logger.info('Ending test_module analysis...')
        return None


# MAIN
#########################
if __name__ == "__main__":
    pass


# GARBAGE
#########################
