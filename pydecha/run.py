# -*- coding: utf-8 -*-
r"""
██████╗ ██╗   ██╗██████╗ ███████╗ ██████╗██╗  ██╗ █████╗
██╔══██╗╚██╗ ██╔╝██╔══██╗██╔════╝██╔════╝██║  ██║██╔══██╗
██████╔╝ ╚████╔╝ ██║  ██║█████╗  ██║     ███████║███████║
██╔═══╝   ╚██╔╝  ██║  ██║██╔══╝  ██║     ██╔══██║██╔══██║
██║        ██║   ██████╔╝███████╗╚██████╗██║  ██║██║  ██║
╚═╝        ╚═╝   ╚═════╝ ╚══════╝ ╚═════╝╚═╝  ╚═╝╚═╝  ╚═╝
-ANSI Shadow-

PYthon DEtector CHAracterization
@ author: bserra / bserra@eso.org
@ organisation: ESO
@ license: MIT
@ project: https://gitlab.com/benoit.serra/pydecha
@ documentation: https://benoit.serra.gitlab.io/pydecha/
__________________________________

Three function modes for the software: cm, doc, char

[doc] documentation
-------------------
Update current documentation using demo_config.yaml config file
  python pydecha/run.py -vvv doc cmos -c demo_config.yaml -du
  cd docs/
  make html

[char] characterization
-----------------------
Run characterization for cmos type using demo_config.yaml
  python pydecha/run.py -vvv char cmos -c demo_config.yaml

Run characterization for ccd type using demo_config.yaml
  python pydecha/run.py -vvv char ccd -c TBD.yaml

[cm] module creation
--------------------
Creating a new module for cmos like sensor:
  python pydecha/run.py -vvv cm cmos demomodule
Creating a new module for ccd:
  python pydecha/run.py -vvv cm ccd demomodule

__________________________________

"""
# IMPORTS
#########################
# import yaml
import subprocess
import argparse
import logging
import sys
import time
import yaml
import os

import methods.pydecha_methods as pdcm
from classes.pipelines import pipelines

# Ignore cryptography blowfish deprecation warning from paramiko
import warnings 
warnings.filterwarnings(action='ignore',module='.*paramiko.*')
# METHODS
#########################
def method(input_method):
    """Example function with types documented in the docstring.

    Args:
        param1 (int): The first parameter.
        param2 (str): The second parameter.

    Returns:
        bool: The return value. True for success, False otherwise.

    """
    return None


def documentation(args):
    # If documentation update was chosen, exit pydecha
    if args.docupdate:
        # Import all modules for this detector type
        pdcm._import_modules(args.dt)
        # Modules are imported, read the yaml config file with the workflow
        setting_dict = pdcm.read_yamlreport('config/'+args.config)
        pdcm._update_modules2(setting_dict)
        # pdcm._update_version(__version__)
        sys.exit('Documentation updated, exiting.')
    else:
        sys.exit('Documentation: Nothing to do.')


def characterization(args):
    # Import all modules for this detector type
    pdcm._import_modules(args.dt)
    # Modules are imported, read the yaml config file with the workflow
    setting_dict = pdcm.read_yamlreport('config/'+args.config)
    if setting_dict is None: sys.exit('YAML not loaded, exiting pydecha')
        
    # Create the save directory
    save_dirname = "outputs/" + time.strftime("%Y%m%d-%H%M%S") + "_" +\
                   args.dt + setting_dict['general']['detector']
    os.mkdir(save_dirname)
    # Initializing the logging
    # log_format = '%(asctime)s - %(name)s - %(funcName)20s \t %(message)s'
    logging.getLogger("matplotlib").setLevel(logging.WARNING)
    logging_level = [logging.ERROR,
                     logging.WARNING,
                     logging.INFO,
                     logging.DEBUG][min(opts_prog.verbosity, 3)]
    log_format = '%(asctime)s - %(name)s - %(funcName)30s \t %(message)s'
    logging.basicConfig(level=logging_level, format=log_format,
                        datefmt='%Y-%m-%d %H:%M:%S',
                        filename=save_dirname+"/"+tstart+"_pydecha.log")

    # If user wants the log in stdout AND in file, use the three lines below
    stream_stdout = logging.StreamHandler(sys.stdout)
    stream_stdout.setFormatter(logging.Formatter(log_format))
    logging.getLogger().addHandler(stream_stdout)

    # Add the version in the dict to track the outputs of the pipelines
    setting_dict['general']['version'] = __version__
    setting_dict['general']['savepath'] = save_dirname+"/"
    setting_dict['general']['tstart'] = tstart
    # Initialize the pipelines
    characterization_pipelines = pipelines(setting_dict)
    # Start the pipelines
    results_dict = characterization_pipelines.start()

    tend = time.strftime("%Y-%m-%d_%H-%M-%S")
    results_dict['general']['tend'] = tend
    system_infos = pdcm._get_systeminfos()
    results_dict['system_infos'] = system_infos
    # Write the settings back in the outputs for tracking
    with open(''.join([setting_dict['general']['savepath'],
                       tstart,
                       '_config.yaml']),
              'w') as outfile:
        yaml.dump(results_dict, outfile, default_flow_style=False)


def modules(args):
    pdcm.create_module(args.dt, args.name)


# MAIN
#########################
if __name__ == "__main__":

    __all__ = []
    __appname__ = 'PYDECHA'
    __author__ = 'Benoit Serra'
    __author_email__ = 'bserra@eso.org'
    __pkgname__ = 'pydecha'
    __version__ = subprocess.check_output(["git",
                                           "describe",
                                           "--long"]).strip().decode()

    tstart = time.strftime("%Y-%m-%d_%H-%M-%S")
    t0 = time.time()

    log_format = '%(asctime)s - %(name)s - %(funcName)30s \t %(message)s'

    # Initializing the general parser
    parser = argparse.ArgumentParser(
            formatter_class=argparse.RawTextHelpFormatter,
            description=__doc__)

    # Add arguments to parse
    parser.add_argument('-V', '--version', action='version',
                        version=__appname__+', version {version}'
                        .format(version=__version__))
    parser.add_argument('-v', '--verbosity',
                        action='count', default=0,
                        help='Increase output verbosity (-v/-vv/-vvv)')

    subparsers = parser.add_subparsers()
    # Add subparser for documentation operations
    detectors_list = ['ccd', 'cmos', 'saphira']
    parser_doc = subparsers.add_parser('doc')
    parser_doc.add_argument('dt', type=str,
                            choices=detectors_list,
                            help='Detector type')
    parser_doc.add_argument('-du', '--docupdate',
                            action='store_true')
    parser_doc.add_argument('-c', '--config', type=str, required=True,
                            help='Configuration file to load (json)')
    parser_doc.add_argument('-dt', '--detectortype', type=str,
                            choices=detectors_list,
                            help='Detector type')
    parser_doc.set_defaults(func=documentation)

    # Add subparser for modules operations
    parser_cm = subparsers.add_parser('cm')
    parser_cm.add_argument('dt', type=str,
                           choices=detectors_list,
                           help='Detector type')
    parser_cm.add_argument('name', type=str)
    parser_cm.set_defaults(func=modules)

    # Add subparser for characterization operations
    parser_char = subparsers.add_parser('char')
    parser_char.add_argument('dt', type=str,
                             choices=detectors_list)
    parser_char.add_argument('-c', '--config', type=str, required=True,
                             help='Configuration file to load (json)')
    parser_char.set_defaults(func=characterization)

    # Parse arguments
    opts_prog = parser.parse_args()

    if hasattr(opts_prog, 'func'):
        opts_prog.func(opts_prog)
    else:
        sys.exit("""No mode chosen for pydecha, to access help use:
  python pydecha/run.py -h""")

    logger = logging.getLogger('pydecha')
    logger.info(__appname__+', version {version}'.format(version=__version__))
    t1 = time.time()
    logger.info('Executed in '+str(t1-t0))
# GARBAGE
#########################
