# -*- coding: utf-8 -*-
"""
Created on Fri May 10 10:02:57 2019

@author: bserra

Reminder
----------
_PEP 484:
https://www.python.org/dev/peps/pep-0484/
----------
"""
# IMPORTS
#########################
import logging

# import acquisition as pdc_acq
# import analysis as pdc_ana
# import analysis as pdc_rep
# from modules.cmos.dark.dark_analysis import dark_analysis


# METHODS
#########################
class pipelines(object):
    """Example function with types documented in the docstring.

    Args:
        param1 (int): The first parameter.
        param2 (str): The second parameter.

    Returns:
        bool: The return value. True for success, False otherwise.

    """

    def __init__(self, input_dict):
        self.settings = input_dict
        self.logger = logging.getLogger('pydecha_pipelines')
        # self.dark = dark_analysis.dark({'datapath': path+data})

    def start(self):
        """"""
        self.logger.info('Starting the pipelines...')
        characterization_workflow = [self.acquisition,
                                     self.analysis,
                                     self.report]
        for module_name, module_workflow in self.settings['workflow']['list'].items():
            self.logger.info(' --------- '+module_name+' --------- ')
            for idx, test in enumerate(module_workflow):
                if test is True:
                    self.logger.info(characterization_workflow[idx])
                    characterization_workflow[idx](module_name)
                else:
                    self.logger.info(str(characterization_workflow[idx])+" set to False")

        return self.settings

    def acquisition(self, module_name):
        """"""
        self.logger.info('Acquisition module starts...')
        # for module in analysis_workflow['list'].keys():
        module_dict = self.settings['acquisition']['list'][module_name]
        acquisition_module = module_dict['module_name'].module(module_name,
                                                            self.settings)
        results = acquisition_module.start()
        if results is not None:
            module_dict.update(**results)
            self.settings['acquisition']['list'][module_name].update(**module_dict)
        acquisition_module._close_module()
        self.logger.info('Acquisition module ends...')
        return None

    def analysis(self, module_name):
        """"""
        self.logger.info('Analysis module starts...')
        # for module in analysis_workflow['list'].keys():
        module_dict = self.settings['analysis']['list'][module_name]
        analysis_module = module_dict['module_name'].module(module_name,
                                                            self.settings)
        results = analysis_module.start()
        if results is not None:
            module_dict.update(**results)
            self.settings['analysis']['list'][module_name].update(**module_dict)
        analysis_module._close_module()
        self.logger.info('Analysis module ends...')
        return None

    def report(self, module_name):
        """"""
        self.logger.info('Reporting module starts...')
        self.logger.info('Reporting module ends...')
        return None


# MAIN
#########################
if __name__ == "__main__":
    pass


# GARBAGE
#########################
