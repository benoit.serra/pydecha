# -*- coding: utf-8 -*-
"""
Created on Wed May 15 13:57:44 2019

@author: bserra

Reminder
----------
_PEP 484:
https://www.python.org/dev/peps/pep-0484/
----------
"""
# IMPORTS
#########################
import sys
import paramiko
import os
import time

# METHODS
#########################
def method(input_method):
    """Example function with types documented in the docstring.

    Args:
        param1 (int): The first parameter.
        param2 (str): The second parameter.

    Returns:
        bool: The return value. True for success, False otherwise.

    """
    return None


# MAIN
#########################
if __name__ == "__main__":

    acq_scriptname = 'dark_acquisition.sh'
    savepath = '/insroot/irlabf/SYSTEM/DETDATA/'
    raidpath = '/dsraid2/users/bserra/Data/'
    remotefilepath = '/home/irlabf/HxRG_BSERRA/scripts/20190515_test/'
    localfilepath = './'
    ssh_client = paramiko.SSHClient()
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_client.connect(hostname='wfmdc2.hq.eso.org',
                       username='irlabf',
                       password='NPirace_02')

    ftp_client = ssh_client.open_sftp()

    ftp_client.put(localfilepath+acq_scriptname,
                   remotefilepath+acq_scriptname)

    # acq_logname = '2019-05-15_12-48-18_test.txt_cleared'
    changepath = 'cd '+remotefilepath
    scripttounix = 'dos2unix '+acq_scriptname
    startacq = 'source '+acq_scriptname


    command = ';'.join([changepath, scripttounix, startacq])
    (stdin, stdout, stderr) = ssh_client.exec_command(command)
    time.sleep(15)
    (stdin, stdout, stderr) = ssh_client.exec_command(changepath+';ls *.txt_cleared')
    filelist = stdout.read().splitlines()
    ftp_client.chdir(remotefilepath)
    print(filelist)
    for afile in filelist:
        (head, filename) = os.path.split(str(afile))
        print(filename)
        ftp_client.get(afile,
                       localfilepath+str(filename))

    ftp_client.close()

# GARBAGE
#########################
