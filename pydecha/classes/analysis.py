# -*- coding: utf-8 -*-
"""
Created on Wed May 15 13:57:44 2019

@author: bserra

Reminder
----------
_PEP 484:
https://www.python.org/dev/peps/pep-0484/
----------
"""
# IMPORTS
#########################
import sys
import paramiko
import os
import time


# METHODS
#########################
class analysis(object):
    """Example function with types documented in the docstring.

    Args:
        param1 (int): The first parameter.
        param2 (str): The second parameter.

    Returns:
        bool: The return value. True for success, False otherwise.

    """
    def __init__(self, dict1, dict2):
        dict1.update(**dict2)
        self.information = dict1
        self.parameters = dict1['module_params']

    def save_param(self, var_name: str, var_value):
        self.information['output'] = {var_name: var_value}


# MAIN
#########################
if __name__ == "__main__":

    pass

# GARBAGE
#########################
