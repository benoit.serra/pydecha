# -*- coding: utf-8 -*-
"""
Created on Wed May 15 13:57:44 2019
@author: bserra
"""
# IMPORTS
#########################
import sys
import paramiko
import os
import time
import logging
import pandas as pd
import numpy as np
from astropy.io import fits
import methods.plot_methods as plotm
from openpyxl import load_workbook


# METHODS
#########################
class analysis:
    """Analysis block object which defines the common methods between all
    analysis modules.
    """
    def __init__(self, module_name, yaml_dict):
        self.logger = logging.getLogger(module_name)
        # analysis module name
        self.logger.info('Initializing analysis object for ' +module_name)
        self.module_name = module_name
        # Creates the self.module_parameters
        self._parse_yamlconfig(yaml_dict)
        self.tag_plot = self._pydecha_config['workflow']['plots']['tag']
        self._create_savefolder()
        self.filename_excel = (self.outputpath +
                               self.module_parameters['general']['tstart'] +
                               '_filessummary.xlsx')
        # Create the excel file
        try:
            self.excel_writer = pd.ExcelWriter(self.filename_excel,
                                               engine='openpyxl', mode='a')
        except FileNotFoundError:
            self.excel_writer = pd.ExcelWriter(self.filename_excel,
                                               engine='openpyxl', mode='w')
            self.excel_writer.book.create_sheet('Summary')


    def _parse_yamlconfig(self, yaml_dict):
        """Parse the dictionnary from the config file into subset for easier
        handling.
        """
        self._pydecha_config = yaml_dict
        # Detector properties defined in the yaml file
        self.detector_parameters = self._pydecha_config['workflow']['detector']

        self.module = self._pydecha_config['analysis']['list'][self.module_name]
        # Module parameters entered in the yaml file for the module
        self.module_parameters = self.module['module_params'].copy()
        print(self.module_parameters['data_path'])
        self.module_parameters['data_path'] = os.path.abspath(self.module_parameters['data_path'])
        print(self.module_parameters['data_path'])
        # os.path.normpath to switch slash to backslashes if windows path in unix machine
        self.module_parameters.update({'general':self._pydecha_config['general']})
        self.module_parameters.update({'detector': self.detector_parameters})

    def _create_savefolder(self):
        """Create a 'module_name' analysis folder inside of output folder and
        changing savepath to it.
        """
        savepath = self._pydecha_config['general']['savepath']+self.module_name
        os.mkdir(savepath)
        self.outputpath = self._pydecha_config['general']['savepath']
        self.savepath = savepath+'/'

    def _get_moduleoutput(self, module_name):
        """Get the output from a given module.
        """
        return self._pydecha_config['analysis']['list'][module_name]['output']

    def save_param(self, var_name: str, var_value):
        """Save a variable name and a value in the dictionnary that
        can then be used for another module in the pipeline.
        """
        if self.module['output'] is None:
            self.module['output'] = {var_name: var_value}
        else:
            self.module['output'].update({var_name: var_value})

    def save_map(self, data, metadata=None, file_type='FITS'):
        """Save a numpy array in a FITS file, if there are metadata (dict),
        the keywords will be saved in the primaryHDU header.
        """
        if file_type == 'FITS':
            hdu = fits.PrimaryHDU(data)
            if metadata is None:
                pass
            elif isinstance(metadata, dict):
                for keyw, value in metadata.items():
                    keyw = keyw.replace('_','')[:8]
                    try:
                        hdu.header[keyw.upper()] = str(value)
                    except ValueError:
                        hdu.header[keyw.upper()] = str(os.path.basename(value))
            hdu.writeto(self.savepath+self.module_name+'_results.fits')

    def save_fig(self, fig, savename):
        """Save a matplotlib.figure object as a png.
        """
        import matplotlib.pyplot as plt
        abs_filename = (self.savepath +
                        "/"+self.module_parameters['general']['tstart'] +
                        '_'+self.module_name +
                        '-'+savename+'.png')
        if self.tag_plot is True:
            plotm.tag_plot(fig, self.module_parameters)
        print (self.module_parameters)
        plt.savefig(abs_filename)#,
        #            metadata=self.module_parameters)


    def print_output(self):
        """Method to display a dictionnary properly"""

    def clean_output(self):
        """Method to format an output dictionnary if needed
        DOES NOTHING FOR NOW"""
        copy_dict = self.module.copy()
#        for key, value in copy_dict.items():
#            if key in self.general_infos.keys():
#                self.module_parameters.pop(key)
        return copy_dict

    def get_dataset_info(self, file_list, file_type='FITS', parameters = None, save=True):
        """Method to get information from a list of FITS files into a pandas
        DataFrame and saving it to excel in a self.module_name sheet

        :param: file_list : list : list of FITS files
        :param: file_type: str [FITS]
        :param: save: bool
        """

        if file_type == 'FITS':
            import astropy.io.fits as fits

            # column_names = ['filename',
            #                 'type',
            #                 'Mode',
            #                 'DIT',
            #                 'VRESET',
            #                 #'Det_Temperature',
            #                 'NAXIS1',
            #                 'NAXIS2']
            index = range(len(file_list))

            for idx, filename in enumerate(file_list):

                header = fits.getheader(filename)
                if filename.split('_')[-1] == 'STDEV.fits':
                    data_type = 'std-dev'
                else:
                    data_type = ''

                saved_params = [['filename', filename],
                                ['type', data_type],
                                ['mode', header['HIERARCH ESO DET READ CURNAME']],
                                ['NAXIS1', header["NAXIS1"]],
                                ['NAXIS1', header["NAXIS2"]]]

                if isinstance(parameters, list):
                    [saved_params.append([param.replace('HIERARCH ESO ',''), header[param]]) for param in parameters]
                saved_params = np.array(saved_params)

                if idx == 0:
                    frame_info = pd.DataFrame(index=index,
                                              columns=saved_params[:, 0])

                frame_info.loc[idx] = saved_params[:, 1]
                # frame_info.loc[idx] = [filename,
                #                        data_type,
                #                        header["HIERARCH ESO DET READ CURNAME"],
                #                        float(header["HIERARCH ESO DET SEQ1 DIT"]),
                #                        float(header["HIERARCH ESO DET CLDC1 DC5"]),
                #                        #float(header["HIERARCH ESO INS T002 INPUT B"]),
                #                        int(header["NAXIS1"]),
                #                        int(header["NAXIS2"])]
            self.dataset = frame_info
            if save is True:
                self._dataset_to_excel()
            return frame_info

    def _dataset_to_excel(self):

        # Try appending data to it
        try:
            self.dataset.to_excel(self.excel_writer, sheet_name=self.module_name)
        except FileNotFoundError:
            # file does not exist yet, we will create it
            self.logger.info('Excel file not found')
            pass

    def _close_module(self):
        self.excel_writer.save()

#    def save_fig(self, fig, savename, dictionnary):
#        import matplotlib.pyplot as plt
#        abs_filename = (self.savepath['savepath'] +
#                        "/"+dictionnary['tstart'] +
#                        '_'+self.module_name +
#                        '-'+savename+'.png')
#        plotm.tag_plot(fig, self.module_parameters)
#        plt.savefig(abs_filename,
#                    metadata=dictionnary)

# MAIN
#########################


# GARBAGE
#########################
