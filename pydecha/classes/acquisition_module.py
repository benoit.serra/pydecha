# -*- coding: utf-8 -*-
"""
Created on Fri May 28 10:24:00 2021
@author: bserra
"""
# IMPORTS
#########################
import paramiko
import json
import time
import logging
import sys
import os
from asyncua import Client, Node, ua
import asyncio
import datetime
import re

# METHODS
#########################
class acquisition:
    """"""
    def __init__(self, module_name, yaml_dict):
        """Initialisation of the logger with timestamps and start
        ssh session"""
        logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)
        self.logger = logging.getLogger(__name__)
        #self.ssh_session = self._init_session(host, user)
        self.module_name = module_name
        self._parse_yamlconfig(yaml_dict)
        
    def _parse_yamlconfig(self, yaml_dict):
        """Parse the dictionnary from the config file into subset for easier
        handling.
        """
        self._pydecha_config = yaml_dict
        # Detector properties defined in the yaml file
        self.detector_parameters = self._pydecha_config['workflow']['detector']

        self.module = self._pydecha_config['acquisition']['list'][self.module_name]
        # Module parameters entered in the yaml file for the module
        if self.module['module_params'] is not None:
            self.module_parameters = self.module['module_params'].copy()
        else:
            self.module_parameters = {}
        # os.path.normpath to switch slash to backslashes if windows path in unix machine
        self.module_parameters.update({'general':self._pydecha_config['general']})
        self.module_parameters.update({'detector': self.detector_parameters})

    def _init_session(self):
        """
        Create SSH connection and returns it, return None if connection
        cannot be established
        Open a ssh_ngc connection with the LLCU machine to control NGC.
        host: ngcselex
        username: nirmos
        key: '/home/dalvarez/.ssh/wmodcir2_dalvarez'
        key: paramiko.RSAKey.from_private_key_file(os.path.abspath("ngcselex/benoit_laptop"), password='irace_02')
        """
        host = self._pydecha_config['workflow']['dws']['host']
        user = self._pydecha_config['workflow']['dws']['user']
        try:
            # Create the client
            self.ssh_ngc = paramiko.SSHClient()
            # When connecting to the server, it usually ask if we
            # want to save it, use AutoAddPolicy for that.
            self.ssh_ngc.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            # Connecting using a stored private key would not work...
            # maybe on dsraid2 it would work, to test
            self.ssh_ngc.connect(hostname=host,
                                 username=user,
                                 password='NPirace_02',
                                 look_for_keys=False,
                                 timeout=5000)
            self.logger.info("Established connection with "+host+" as "+user)
        except:
            # If it is not connected the ssh_ngc is None
            self.ssh_ngc = None
            self.logger.info("Connection with "+host+" as "+user+" failed")
        return self.ssh_ngc
    
    async def send_command(self, url, command):
        # If I do not put the tieout 10 I will have a concurrent.futures._base.TimeoutError from async with Client(url=url) as client when trying to connect.
        async with Client(url=url, timeout=30) as client:
            # Client has a few methods to get proxy to UA nodes that should always be in address space such as Root or Objects
            # Node objects have methods to read and write node attributes as well as browse or populate address space
            self.logger.info('Children of root are: %r', await client.nodes.root.get_children())
            uri = 'http://examples.freeopcua.github.io'
            idx = await client.get_namespace_index(uri)
            # This is the node of the method you can read on UAExpert
            # Previously it was define with integers, now it is a string.
            methods = client.get_node("ns="+str(idx)+";s=internalMethods")
            # This is the InputArguments of the method
            sendcommand_method = client.get_node("ns="+str(idx)+";s=sendCommand")
            response = await methods.call_method(sendcommand_method, command)
            return response
    
    def _send_command(self, command):
        """Send a command to the LLCU"""
        # Try to send it, exception if error message
        try:
            # Input, Output, Error from the command send to the LLCU
            stdin, stdout, stderr = self.ssh_ngc.exec_command(command)
            stdout.channel.recv_exit_status()
            # Read the output
            temp = stdout.readlines()[0].strip()
            self.logger.info("%s -> %s ", command, temp)
            return temp
        except Exception as exc_object:
            self.logger.info('Error on line {}'.format(sys.exc_info()[-1].tb_lineno),
                             type(exc_object).__name__,
                             exc_object)
            return 'Error while sending command...'

    def get_status(self):
        """Compare the string from the NGC with the default one"""
        NGC_status = "DET.EXP.STATUS \"integrating\""
        NGC_success = "DET.EXP.STATUS \"success\""
        # Continuously check the status, when it is 'success' it stops
        print (NGC_status+"\r")
        while (NGC_status != NGC_success):
            self.logger.info(NGC_status)
            time.sleep(1)
            NGC_status = self._send_command("ngcbCmd status DET.EXP.STATUS")
        print(NGC_status+"\r")
        return "Exposure ended."

    def datenow(self):
        """Return the date 
        ex: 20210510_090825 for 10th of March 2021 at 9:08 AM"""
        date = datetime.datetime.now()
        return date.strftime("%Y%m%d-%H%M%S_")

    def filename(self):
        """Return the complete filename that can be used for acquisition"""
        det_metadata = self._pydecha_config['general']['detector']
        return self.datenow()+det_metadata

    def save_param(self, var_name: str, var_value):
        """Save a variable name and a value in the dictionnary that
        can then be used for another module in the pipeline.
        """
        if self.module['output'] is None:
            self.module['output'] = {var_name: var_value}
        else:
            self.module['output'].update({var_name: var_value})

    def setup_ngc(self, commands):
        re_pattern = r'([^"]*)'
        for line in ['DET.CFGPATH',
                     'DET.SYSCFG',
                     'DET.READ.CURNAME',
                     'DET.CLDC1.FILE',
                     'DET.SEQ1.CLKFILE',
                     'DET.SEQ1.PRGFILE']:
            self.logger.info(line)
            test = self._send_command(f'ngcbCmd status {line}')
            self.logger.info(test)
            if line != 'DET.CFGPATH':
                self.save_param(line, os.path.basename(re.findall(re_pattern, test)[2]))
            else:
                self.save_param(line, os.path.dirname(re.findall(re_pattern, test)[2]))

        # For each line in the script, send it to the
        for line in commands:
            print (line)
            if line.startswith('wait'):
                sleepTime = int(line.split(' ')[1])
                self.logger.info(f'Waiting for {sleepTime} seconds')
                time.sleep(sleepTime)
            else:
                test = self._send_command(line)
                # If the line is starting the acquisition use get_status
                if line == 'ngcbCmd start':
                    self.logger.info("Acquisition started")
                    self.logger.info(self.get_status())
                self.logger.info(test)
                # Between each command, small nap of 1s
                time.sleep(1)
        
    def setup_instrument(self, commands):
        """
        Need the following commands
        - wait <time>
        - stab <variable> <value> <time>
        """
        host = self._pydecha_config['workflow']['rbpi']['host']
        # Creating the URL from the ip address
        url = f'opc.tcp://{host}:4500/freeopcua/server/'
        loop = asyncio.get_event_loop()
        for line in commands:
            print (line)
            if line.startswith('wait'):
                sleepTime = int(line.split(' ')[1])
                self.logger.info(f'Waiting for {sleepTime} seconds')
                time.sleep(sleepTime)
            else:
                response = loop.run_until_complete(self.send_command(url, line))
                print (response)

    #def send_command(self, client, command):
    #    stdin, stdout, stderr = ssh_ngc.exec_command(command)
    #    stdout.channel.recv_exit_status()
    #    temp = stdout.readlines()[0].strip()
    #    self.logger.info("%s -> %s ", command, temp)

    #def set_bias(self,  detector_id, bias_tag, bias_value):
    #    """Set the bias, DO NOT USE YET"""
    #    command = "DET.CLDC"+str(detector_id)+"."+str(bias_tag)+" "+str(bias_value)
    #    response = self._send_command(command)
    #    return response

    #def set_parameter(self,  detector_id, bias_tag, bias_value):
    #    """Set clock parametre, DO NOT USE YET"""
    #    command = "DET.CLDC"+str(detector_id)+"."+str(bias_tag)+" "+str(bias_value)
    #    response = self._send_command(command)
    #    return response
    
    def clean_output(self):
        """Method to format an output dictionnary if needed
        DOES NOTHING FOR NOW"""
        copy_dict = self.module.copy()
#        for key, value in copy_dict.items():
#            if key in self.general_infos.keys():
#                self.module_parameters.pop(key)
        return copy_dict
        
    def _close_module(self):
        # Once it is over, close the connection
        self.ssh_ngc.close()
        return None

#    def save_fig(self, fig, savename, dictionnary):
#        import matplotlib.pyplot as plt
#        abs_filename = (self.savepath['savepath'] +
#                        "/"+dictionnary['tstart'] +
#                        '_'+self.module_name +
#                        '-'+savename+'.png')
#        plotm.tag_plot(fig, self.module_parameters)
#        plt.savefig(abs_filename,
#                    metadata=dictionnary)

# MAIN
#########################


# GARBAGE
#########################
