#!/bin/bash
clear

# File created Mon May 13 13:57:19 2019
## ACQUISITION BASH FILE FOR _TEMPLATE
# Search for '#~' to locate lines you have to check before executing
## Function declarations here
# ----------------------------------------------------------------------
# Function to set DC bias to given value
function set_DC {
  echo "Setting DC${1} to ${2}"
  ngcbCmd setup DET.CLDC1.DC${1} ${2}
  sleep 1
}
# ----------------------------------------------------------------------
# Function to set the filename of the FITS
function set_filename {
  #setup NGC with user defined input parameters
  ngcbCmd setup DET.FRAM.FILENAME $filename
  sleep 1
  echo "Filename = "$filename
  echo " "
}
# ----------------------------------------------------------------------
# Function to set a parameter
function set_parameter {
  echo "Setting ${1} to ${2}"
  ngcbCmd setup ${1} ${2}
  sleep 1	

}
# ----------------------------------------------------------------------
# Function to load the given configuration
function setup_configuration {
  #setup NGC with user defined input parameters
  set_parameter DET.READ.CURNAME $program
  set_parameter DET.SEQ1.DIT $DIT
  set_parameter DET.NDITSKIP $NDITSKIP
  set_parameter DET.NDIT $NDIT
  set_parameter DET.FRAM.FORMAT cube
  set_parameter DET.ACQ1.QUEUE $queue
  set_parameter DET.FRAM.FILENAME $filename

  echo "NGC configuration parameters :" 
  echo "------------------------------"
  echo "NDIT 		= "$NDIT
  echo "NDITSKIP 	= "$NDITSKIP
  echo "DIT 		= "$DIT
  echo "Delay		= "$DITDELAY
  echo "Filename 	= "$filename
  echo "Format 		= "$format
  echo "Queue 		= "$queue
  echo "Read-mode 	= "$program
  echo "NGC Bias/Clock/Program files"
  echo "----------------------------"
  echo "Bias file: "$(ngcbCmd status DET.CLDC1.FILE)
  echo "Clock file: "$(ngcbCmd status DET.SEQ.CLKFILE)
  echo "Program file: "$(ngcbCmd status DET.SEQ.PRGFILE)  
  echo " "
sleep 5
}
# ----------------------------------------------------------------------
# Function to check the status of the acquisition
function check_status {
  test_result="DET.EXP.STATUS \"integrating\""
  i="DET.EXP.STATUS \"success\""
  while [ "$test_result" != "$i" ]
  do
    echo $test_result
    sleep 1
    test_result="$( ngcbCmd status DET.EXP.STATUS )" 
  done
}
# ----------------------------------------------------------------------
# Function to start the exposure
function take_exposure {
  echo "Take an exposure"
  ngcbCmd start
  check_status
  sleep 2
}
# ----------------------------------------------------------------------

########################################################################
# ACQUISITION _TEMPLATE
########################################################################

##~ Constants definition
# Filename extension
timestamp_acquisition="$(date +%s)"
date="$(date +%F_%H-%M-%S)"
file_ext=${timestamp_acquisition}"_"${date}"__TEMPLATE"
save_path=${INS_ROOT}/SYSTEM/DETDATA/${date}
#~ The raid disk path where the data will be copied at the end
raid_path=/dsraid2/users/bserra/Data/

#~ Exposure information
DIT=1
DITDELAY=0.0
NDITSKIP=0
NDIT=1
queue=1
format="cube"
program="SLOW_UP-THE-RAMP"
filename=${date}/${file_ext}
#~ Setup name defined in moni-fits.py 
setup="CRISLER"
# Create a folder in insroot/system/detdata to store the data
mkdir ${save_path}

# moni-fits.py will check file creation and query info from monitoring
# to put into the FITS header
python /home/irlabf/HxRG_BSERRA/scripts/python/moni-fits/moni-fits.py -s $setup &
# Buffer of 2 seconds before starting everything
# if not present, first frame will not have monitoring info
sleep 2

# group everything so it can go to log file
##~ Acquisition config and start.
LOGFILE=${date}"_acq-log.txt"
{

echo "Logfile "${LOGFILE}" acquisition by "${USER}
echo "All FITS data can be found in DETDATA folder of: "${save_path}
echo " "

# Setup configuration using the constants defined in '##~ Constants definition'
setup_configuration
# Set the filename
set_filename
# Begin exposure
take_exposure

echo "Finished"

} 2>&1 | tee $LOGFILE
# back to stand input, finish with log file, taking out the string that
# is repeated non-stop during the integration.
grep -v 'DET.EXP.STATUS \"integrating\"' ${LOGFILE} > ${LOGFILE}

sleep 20
# Copy the data folder on the mounted raid disk
cp -r ${save_path} ${raid_path}
# If used in a terminal exit will close the ssh session
# exit $?


