# -*- coding: utf-8 -*-
"""
_TEMPLATE analysis module
-------------------------------------------------------------------------------

@author: bserra (ESO)\n
@analysis: _TEMPLATE\n
@creation: %(date)

This is a documentation template for the _TEMPLATE analysis module.
Documentation is generated going into docs/ and typing 'make html'

This module can be found in pydecha/modules/_TEMPLATE/_TEMPLATE_analysis.py.
Please modify the docstrings accordingly to provide the users a simple and
detailed explanation of your algorithm for this module.

Table examples
==============

===============  ==============================================================
Table entries    Table values
===============  ==============================================================
Entry 1          Value 1
Entry 2          Value 2
Entry 3          Value 3

Entry 4          Value 4

Entry 5          Value 5
===============  ==============================================================

+------------------------+------------+----------+----------+
| Header row, column 1   | Header 2   | Header 3 | Header 4 |
| (header rows optional) |            |          |          |
+========================+============+==========+==========+
| body row 1, column 1   | column 2   | column 3 | column 4 |
+------------------------+------------+----------+----------+
| body row 2             | Cells may span columns.          |
+------------------------+------------+---------------------+
| body row 3             | Cells may  | - Table cells       |
+------------------------+ span rows. | - contain           |
| body row 4             |            | - body elements.    |
+------------------------+------------+----------+----------+
| body row 5             | Cells may also be     |          |
|                        | empty: ``-->``        |          |
+------------------------+-----------------------+----------+

Code example
============

.. code-block:: python

    import sys

    print('Hello world...')


.. literalinclude:: ../../../pydecha/modules/cmos/_TEMPLATE/_TEMPLATE_analysis.py
    :language: python
    :linenos:
    :lines: 84-87

Useful links
============

ReadTheDocs documentation
https://sphinx-rtd-theme.readthedocs.io/en/latest/index.html
"""
# IMPORTS
#########################
import logging


# METHODS & CLASSES
#########################
class analysis(object):
    """Example class with documented types

    :param setting_test_classes (dict): Input parameters for the analysis.

    :return None: The return value. Nothing for now...
    """
    def __init__(self, settings__TEMPLATE, settings_general):
        self.logger = logging.getLogger('_TEMPLATE_analysis')
        settings__TEMPLATE.update({'savepath': settings_general['savepath'],
                                   'tstart': settings_general['tstart']})
        self.information = settings__TEMPLATE
        self.parameters = settings__TEMPLATE['module_params']

    def start(self):
        """"""
        self.logger.info('Starting _TEMPLATE analysis...')
        # noise code goes here
        print('_TEMPLATE analysis module')
        self.logger.info('Ending _TEMPLATE analysis...')
        return None


# MAIN
#########################
if __name__ == "__main__":
    pass


# GARBAGE
#########################
