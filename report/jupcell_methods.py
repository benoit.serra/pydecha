# -*- coding: utf-8 -*-
"""
Created on Fri Apr  5 13:41:00 2019

@author: serra

v0.0.1 -  Creation
"""

# jupcell_methods.py --- Simple application for generating report
#
# Copyright (C) 2019 Benoit Serra
#
# This file is a program monitoring the instruments connected to it. 
# It may be used and modified with no restriction; raw copies as well 
# as modified versions may be distributed without limitation.

# IMPORTS
#########################
import json
import numpy as np
import glob
import pandas as pd
import matplotlib.pyplot as plt

# METHODS
#########################
def _error_dict():
    """
    Method with the error dictionnary
    """
    ERR_DICT = {1:"Filename not defined",
                2:"Not enough arguments",
                3:"Error while opening YAML file",
                4:"Error while reading YAML file",
                5:"type is not 'markdown' or 'code'",}
    return ERR_DICT


# Methods for generating the python notebook (.ipynb)
# ---------------------------------------------------
def generate_notebook(filename,**kwargs):
    """
    Function that will generate the notebook starting from yaml file
    """
    ERR_DICT = kwargs["errors"]
    # Extract the contents of the yml file for the report generation
    report_dict = srm._read_yamlreport(filename,errors=ERR_DICT)
    
    """ Reminder: SRM Methods
    
    srm._write_pythonnotebook(list_cells, **kwargs):
    srm._insert_cell_latex(string, metadata):
    srm._insert_cell_table(string, metadata):
    srm._insert_cell_markdown(string, metadata):
    srm._insert_cell_text(string, metadata):
    srm._insert_cell_code(string, metadata):
        
    To add a new page:
    list_cell.append(srm._insert_cell_latex("\\newpage"))

    
    """
    # Create the cell dictionnary for the notebook
    cells = srm._create_nbdict()
    # Create a list which will hold all the generated cells
    list_cell = list()
    prefix = ""#"/home/benoit/mnt_renoir"
    reference_run = "initial_check_baseline"
    
    # Extract the data path from yml file
    data_path = prefix+report_dict["general"]["da_path"]
    # For context in reference file L0 data
    data_path2 = prefix+report_dict["general"]["da_path2"]
    # Extract asic id
    asic_id = report_dict["params"]["asic_ID"]
    # Extract Flight Run id
    fr_id = report_dict["general"]["fr_ID"]
    
    # Search for a reference file for taking out the context
    reference_file = glob.glob(data_path+"L1/80K/*reference-qc.h5")[-1]
    #reference_file = glob.glob(data_path2+"L0/80K/*reference-acq.h5")[-1]
    ctx_acq = json.loads(h5.File(reference_file,"r")["__ctx"][()])
    
    # Extract the SCS id from the context for the report
    for i in ctx_acq.keys():
        if ("__SCS_"+str(asic_id) in i) & ("__ID" in i):
            scs_id = ctx_acq[i]
    mcd_path = ctx_acq["SETUP__DPATH"]+"/mcd/"

    """
    _insert_cell_code contains specific already defined cells:
        import - libraries imports
        settings - matplotlib settings
        titlepage - title page
    """
    # Imports for the notebook
    list_cell.append(srm._insert_cell_code("import", None, None))
    # Settings for matplotlib
    list_cell.append(srm._insert_cell_code("settings",None,None))
    srm._set_matplotlib()
    # Create titlepage
    list_cell.append(srm._insert_cell_latex("titlepage",scs=scs_id,fr=fr_id))
    #list_cell.append(srm._insert_cell_image("/home/benoit/Softwares/EUCLID_CPPM_SCSREPORT/2"))
    list_cell.append(srm._insert_cell_latex("\\newpage"))
    # Extract the Operational Temperatures
    OTs = report_dict["params"]["OT"]
    # Extract the runs
    runs = report_dict["runs"].keys()
    #data_path = "/home/benoit/mnt_renoir"+report_dict["general"]["da_path"]

    # Model for table caption
    caption_tab = "Stability of {a} over reported runs"
    # Model for table bias caption
    caption_bias_tab = "Biases loaded for MCD file: {a} @ {b} (taken on "+re.escape(reference_run)+")"
    # Model for table ref (latex)
    label_tab = "tab:{a}"
        
    # List parts of the report
    for part in report_dict["plan"]:
        list_cell.append(srm._insert_cell_latex("\\newpage"))
        # If part in the plan is within the runs in the .yml file        
        if part in report_dict["runs"].keys():
            continue

        # Write a markdown cell with the part name in the python notebook
        list_cell.append(srm._insert_cell_markdown("# "+part.keys()[0], ""))

        if "SCS configuration" in part:
            for OT in OTs:
                table, kwouts = srm._trace_table(data_path, reference_run, prefix+mcd_path, OT, asic_id, "mcd_bias")
                list_cell.append(srm._insert_cell_latex(table, caption=caption_bias_tab.format(a=re.escape(kwouts["mcd"]),b=OT),label=label_tab.format(a=kwouts["mcd"]),cell_type="table"))

        if "Telemetry stability" in part:
                variables = [("DSub [ADU]","chrono_DSub_C_mean"),\
                             ("VReset [ADU]","chrono_VReset_C_mean"),\
                             ("VRefMain [ADU]", "chrono_VRefMain_V_mean")]#,\
                             #("CG-wkf","cg_wkf")]
                # The OTs
                OTs = ["80K","90K","100K","RT"]          

                for variable in variables:
                    table, kwouts = srm._trace_table(data_path, runs, variable, OTs, asic_id, "t_stab")
                    list_cell.append(srm._insert_cell_latex(table, caption=caption_tab.format(a=variable[0]),label=label_tab.format(a=variable[0]),cell_type="table"))

        # List the subparts of that part
        for subpart in part[part.keys()[0]]:
            # Write a markdown cell with the subpart name in the notebook
            if subpart[0] != "_": list_cell.append(srm._insert_cell_markdown("## "+subpart, ""))
                
            if subpart != []:
                # If it is OT stability part
                if subpart == "Operational temperature stability":
                    # Variables we want
                    variables = [("PT9 [K]","R-T_PT100_9"),\
                                 ("SCE [K]","R-T_SCE"),\
                                 ("SCA [K]","R-T_SCA")]#,\
                                 #("CG-wkf","cg_wkf")]
                    # The OTs
                    OTs = ["80K","90K","100K","RT"]
                
                    for variable in variables:
                        table, kwouts = srm._trace_table(data_path, runs, variable, OTs, asic_id, "t_stab")
                        list_cell.append(srm._insert_cell_latex(table, caption=caption_tab.format(a=variable[0]),label=label_tab.format(a=variable[0]),cell_type="table"))
                                    
                if subpart[1:] in runs:
                    run = [subpart[1:]]

                    # Dictionary with all elements of the table for this run
                    for idx, table_desc in enumerate(report_dict["runs"][run[0]]["table"]):                     
                        # Variable from all_stats.csv that need to be displayed
                        variable = table_desc["dset"]
                        # OTs for the table
                        OTs = table_desc["OT"]
                        # Generate table as a latex string
                        table, kwouts = srm._trace_table(data_path, run, variable, OTs, asic_id, "stats", table_info=table_desc)
                        # Create raw cell with the table, with caption and label
                        list_cell.append(srm._insert_cell_latex(table, caption=caption_tab.format(a=table_desc["title"]),label=label_tab.format(a=subpart[1:]+str(idx)),cell_type="table"))
                        # Insert comments for said table                        
                        list_cell.append(srm._insert_cell_latex(table_desc["comts"]))
                                            
                    # Dictionary with all elements of the table for this run
                    for idx, map_desc in enumerate(report_dict["runs"][run[0]]["maps"]):                     
                        # Variable from all_stats.csv that need to be displayed
                        variable = map_desc["dset"]
                        # OTs for the table
                        OTs = map_desc["OT"]
                        # Generate table as a latex string
                        figure_list = srm._trace_plot(data_path2, run, map_desc, asic_id, fr_id=fr_id, scs_id=scs_id)
                        # Create raw cell with the table, with caption and label
                        [list_cell.append(cell_content) for cell_content in [srm._insert_cell_latex(figure, caption=caption_tab.format(a=map_desc["title"]),label=label_tab.format(a=subpart[1:]+str(idx)+"_"+str(idx_fig)),cell_type="figure") for idx_fig, figure in enumerate(figure_list)]]
                        # Insert comments for said table                        
                        list_cell.append(srm._insert_cell_latex(map_desc["comts"]))


            if subpart in report_dict["runs"].keys():
                list_cell.append(srm._insert_cell_markdown("### "+str(report_dict["runs"][subpart].keys()), ""))                

    cells["cells"] = list_cell
    cells.update(srm._insert_nb_metadata())
    #print cells
    srm._write_pythonnotebook(cells)
    
    # End
    return None

def _read_yamlreport(filename, **kwargs):
    """
    Method for reading input yaml file
    """
    import yaml
    ERR_DICT = kwargs["errors"]

    try:
        with open(filename, 'r') as stream:
            try:
                yaml_report = yaml.load(stream)
            except yaml.YAMLError as exc:
                print (ERR_DICT[4],":",exc)
    except (IOError, RuntimeError, TypeError, NameError) as exc:
        print (ERR_DICT[3],":",exc)
            
    return yaml_report
        
def _write_pythonnotebook(dict_nb, **kwargs):
    """
    Method for writing all written cells into a python notebook
    """
    import json
    with open('test.ipynb', 'w') as fp:
        json.dump(dict_nb, fp, sort_keys=True, indent=1)
    return None

def _create_nbdict():
    """"""
    return json.loads("""{"nbformat": 4,"nbformat_minor": 0}""")
    
def _insert_nb_metadata():
    """
    """
    metadata = """{"metadata": {
"kernelspec": {
    "display_name": "Python 2",
    "language": "python",
    "name": "python2"
    },
"language_info": {
    "codemirror_mode": {
    "name": "ipython",
    "version": 2
    },
    "file_extension": ".py",
    "mimetype": "text/x-python",
    "name": "python",
    "nbconvert_exporter": "python",
    "pygments_lexer": "ipython2",
    "version": "2.7.13"
},
"toc": {
    "colors": {
        "hover_highlight": "#DAA520",
        "running_highlight": "#FF0000",
        "selected_highlight": "#FFD700"
        },
    "moveMenuLeft": true,
    "nav_menu": {
        "height": "49px",
        "width": "252px"
        },
    "navigate_menu": true,
    "number_sections": true,
    "sideBar": true,
    "threshold": 4,
    "toc_cell": false,
    "toc_section_display": "block",
    "toc_window_display": false
    }
}}"""
    return json.loads(metadata)    
    
def _insert_cell(cell_type,execution_count,metadata):
    """
    Method for creating an empty working cell
    """
    if cell_type in ["markdown","code"]:
        if cell_type == "code":
            cell = """{
    "cell_type": \""""+cell_type+"""\",
    "execution_count": null,
    "metadata": {
        "collapsed": false
    },
    "outputs": [],
    "source": ["This is a """+cell_type+""" cell"]
    }"""
        # No output or execution code for markdown cells in nb
        if cell_type == "markdown":
            cell = """{
    "cell_type": \""""+cell_type+"""\",
    "metadata": {
        "collapsed": false
    },
    "source": ["This is a """+cell_type+""" cell"]
    }""" 
    
    else:
        sys.exit("cell_type="+cell_type, ERR_DICT[5])
    return json.loads(cell)

def _insert_cell_text(string, metadata):
    """
    Method for inserting cell with basic text string in it
    """
    cell = """{
    "cell_type": \"raw\",
    "metadata": {
        "collapsed": false
    },
    "source": [\""""+string+"""\"]
    }""" 

    return json.loads(cell)
    
def _insert_cell_markdown(string, metadata):
    """
    Method for inserting cell with markdown string in it
    """
    cell = """{
    "cell_type": \"markdown\",
    "metadata": {
        "collapsed": false
    },
    "source": [\""""+string+"""\"]
    }""" 

    return json.loads(cell)
    
def _insert_cell_image(link):
    """
    Method for inserting cell with latex image embedding string in it
    """
    cell = json.loads("""{
    "cell_type": \"raw\",
    "metadata": {
        "collapsed": false
    },
    "source": []
    }""")
    cell["source"]="\\includegraphics{\""+link+"\"}"
    return cell

def _insert_cell_latex(string, **kwargs):
    """
    Method for inserting cell with latex string in it
    """
    cell_type = None
    cell = json.loads("""{
    "cell_type": \"raw\",
    "metadata": {
        "collapsed": false
    },
    "source": []
    }""")
    try:
        scs_id = kwargs["scs"]
        fr_id = kwargs["fr"]
    except:
        pass
    
    try:
        if kwargs["cell_type"] == "table":
            caption = kwargs["caption"]
            label = kwargs["label"]
            cell["metadata"].update({'referenced':True,'table':True,'caption':caption,'label':label})
        if kwargs["cell_type"] == "figure":
            caption = kwargs["caption"]
            label = kwargs["label"]
            cell["metadata"].update({'referenced':True,'widefigure':True,'caption':caption,'label':label})            
    except:
        pass
        
    def titlepage(cell,scs_id,fr_id):
        """
        Method for inserting cell with latex string in it
        """
        with open('scsreport_titlepage.tex', 'r') as myfile:
            source=myfile.read()
        cell["source"]=source.decode('utf-8', 'ignore').replace("SCS_ID",scs_id).replace("FR_ID",str(fr_id))
        return cell        
    
    if string == "titlepage":
        cell=titlepage(cell,scs_id,fr_id)
    else:
        cell["source"]=string
    
    return cell

def _insert_cell_code(cell_type, string, metadata):
    """
    Method for inserting cell with latex string in it
    """
    cell = json.loads("""{
    "cell_type": \"code\",
    "execution_count": null,
    "metadata": {
        "collapsed": false
    },
    "outputs": [],
    "source": []
    }""")

    def plot(string, plot_type, metadata):
        """
        Method for inserting cell with latex string in it
        """
        return None
     
    def image(string, plot_type, metadata):
        """
        Method for inserting cell with latex string in it
        """
        return None
    
    def settings(cell):
        """
        """
        source = """# Settings for matplotlib
srm._set_matplotlib()"""
 
        cell["source"]=source
        return cell
        
    def imports(cell):
        """
        Method for inserting cell with latex string in it
        """
        source = """# Importing modules\n
%matplotlib inline\n
# Figures haute qualite prend plus de temps pour compiler le notebook
#%config InlineBackend.figure_format = 'svg'

from IPython.display import Latex, HTML, display, Markdown
from IPython.display import Image

import matplotlib.pyplot as plt

import matplotlib.dates as mdates
import datetime
import matplotlib.transforms as transforms

from mpl_toolkits.axes_grid1 import make_axes_locatable

import numpy as np
import pandas as pd
import h5py as h5
import glob
import json
import os

import scs_report_methods as srm"""

        cell["source"]=source
        return cell
    

        
    if cell_type == "import":
        cell = imports(cell)
    if cell_type == "settings":
        cell = settings(cell)
        
    return cell