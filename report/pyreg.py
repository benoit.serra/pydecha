# -*- coding: utf-8 -*-
"""
Created on Fri Apr  5 12:08:00 2019

@author: serra

v0.0.1 -  Creation
"""

# pyreg.py --- Simple application for generating report
#
# Copyright (C) 2019 Benoit Serra
#
# This file is a program monitoring the instruments connected to it.
# It may be used and modified with no restriction; raw copies as well
# as modified versions may be distributed without limitation.

# IMPORTS
#########################
import argparse
import os
import sys

import pyreg_methods as prgm
import jupcell_methods as jupy
import docx_methods as docm
from docx import Document
from docx.shared import Inches


progname = os.path.basename(sys.argv[0])
progversion = "0.0.1"

# METHODS
#########################


# MAIN
#########################
if __name__ == "__main__":

    yaml_report = None
    parser = argparse.ArgumentParser(description='Parser of arguments for the report generation.')
    parser.add_argument('dpath', metavar='dpath', type=str, nargs='+',
                        help='The data path used for the report generator')
    parser.add_argument('config', metavar='yaml_report', type=str, nargs='+',
                        help='The YAML file used for the report generation')

    tpath = "Z:/users/bserra/Softwares/Validation/pyreg/data/templates/office/"
    ERR_DICT = jupy._error_dict()

    args = parser.parse_args()
    print(args.dpath)

    # prgm.plot_fields(args.dpath[0],
    #                 ['T001',
    #                  'T002',
    #                  'UPS001',
    #                   'C001'])

    if args.config is not None:
        report_dict = prgm._read_yamlreport(
                'data/templates/yaml/report_desy.yml',
                errors=ERR_DICT)
        list_parts = [list(partname)[0] for partname in report_dict['plan']]
        docm.generate_word(tpath+'20190408_ESO-Template02.docx',
                           list_parts, errors=ERR_DICT)
        # jpcm.generate_notebook(args.yaml_report,errors=ERR_DICT)
    else:
        sys.exit('Exit.')

# GARBAGE
#########################
