# -*- coding: utf-8 -*-
"""
Created on Fri Apr  5 13:41:00 2019

@author: serra

v0.0.1 -  Creation
"""

# pyreg_methods.py --- Simple application for generating report
#
# Copyright (C) 2019 Benoit Serra
#
# This file is a program monitoring the instruments connected to it. 
# It may be used and modified with no restriction; raw copies as well 
# as modified versions may be distributed without limitation.

# IMPORTS
#########################
import pandas as pd
import glob
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams

#rcParams['figure.figsize'] = 10, 5
rcParams['figure.dpi'] = 100

# METHODS
#########################
def _read_yamlreport(filename, **kwargs):
    """
    Method for reading input yaml file
    """
    import yaml
    ERR_DICT = kwargs["errors"]

    try:
        with open(filename, 'r') as stream:
            try:
                yaml_report = yaml.load(stream)
            except yaml.YAMLError as exc:
                print (ERR_DICT[4],":",exc)
    except (IOError, RuntimeError, TypeError, NameError) as exc:
        print (ERR_DICT[3],":",exc)
            
    return yaml_report

def extract_monitoring(dpath: str):
    """"""
    time_fmt = '%Y-%m-%d_%H-%M-%S'
    files = glob.glob(dpath+'*.txt')

    for idx, file in enumerate(files):
        print (file)
        txt = pd.read_csv(file,sep='\t', dtype={'Time': object},header=0,comment='#')
        if idx == 0:
            df = txt
        else:
            df = pd.concat([df,txt],ignore_index=True) 

    df_comments = df[df['Comments'].notna()]
    df_monitoring = df[df['Comments'].isna()]
        
    df_monitoring['Time'] = pd.to_datetime(df_monitoring.Time, format=time_fmt)
    df_comments['Time'] = pd.to_datetime(df_comments.Time, format=time_fmt)
            
    return df_monitoring, df_comments

def plot_fields(dpath: str,
                fields: list):

    dataframe, comments = extract_monitoring(dpath)
    fields = np.unique(np.array([field[:4] for field in fields]))
    
    for instrument in fields:
        figure, axes = plt.subplots(1,1)
        figure.canvas.set_window_title(instrument)
        for field_name in dataframe.columns.values.tolist():
            if instrument in field_name:
                axes.plot(dataframe['Time'], dataframe[field_name],label=field_name)
                axes.grid(True)
        axes.legend(fontsize=12)
        plt.xticks(rotation=45)
        plt.tight_layout()
   
    plt.show()
    
# MAIN
#########################
if __name__ == "__main__":
    print ('Loading methods...')

# GARBAGE
#########################  