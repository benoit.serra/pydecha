**PY**thon

**DE**tector

**CHA**racterization

Author: @benoit.serra

Three modes for the software

Documentation
-------------

Online documentation available here: https://benoit.serra.gitlab.io/pydecha/

    python pydecha/run.py doc cmos -c demo_config.yaml -du
    cd docs/
    make html

Characterization
----------------

    python pydecha/run.py char cmos -c demo_config.yaml

Module creation
---------------

    python pydecha/run.py cm cmos MODULENAME
    python pydecha/run.py cm ccd MODULENAME