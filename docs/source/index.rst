.. pydecha documentation master file, created by
   sphinx-quickstart on Thu May 16 15:44:10 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pydecha's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. toctree::
   :caption: Introduction
   :maxdepth: 4

   introduction/pydecha
   introduction/installation
   introduction/first_steps

.. toctree::
   :caption: Characterization modules
   :maxdepth: 8

   modules/new_module
   modules/modules_ccd
   modules/modules_cmos

.. toctree::
   :caption: About
   :maxdepth: 4
   
   about/acronyms
   about/authors
   about/changelog
   about/faq
   about/license
   about/references
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
