# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# http://www.sphinx-doc.org/en/master/config

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.

import sys
import os
from pathlib import Path
from setuptools.config import read_configuration

sys.path.insert(0, os.path.abspath(os.path.join('..', '..')))
sys.path.insert(0, os.path.abspath(os.path.join('..', '..', 'pydecha')))
sys.path.insert(0, os.path.abspath(os.path.join('..', '..', 'pydecha', 'methods')))
sys.path.insert(0, os.path.abspath(os.path.join('..', '..', 'pydecha', 'modules')))

print (sys.path)
#sys.path.insert(0, os.path.abspath('../../pydecha'))
#sys.path.insert(0, os.path.abspath('../../pydecha/methods'))
#sys.path.insert(0, os.path.abspath('../../pydecha/modules'))

# -- Project information -----------------------------------------------------

# project = 'pydecha'
# copyright = '2019, Benoit Serra'
# author = 'Benoit Serra'

# Read 'setup.cfg' file
parent_folder = Path(__file__).parent
setup_cfg_filename = parent_folder.joinpath("../../setup.cfg").resolve(strict=True)  # type: Path
metadata = read_configuration(setup_cfg_filename)["metadata"]  # type: dict

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = ['sphinx.ext.autodoc',
              'sphinx.ext.todo',
              'sphinx.ext.napoleon'
]

highlight_language = 'python3'

autodoc_member_order = 'bysource'
todo_include_todos = True

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

# General information about the project.
project = metadata["name"]
copyright = '2019, European Southern Observatory'
author = metadata["author"]

# The version info for the project you're documenting, acts as replacement for
# |version| and |release|, also used in various other places throughout the
# built documents.
#
# The short X.Y version.
version = metadata["version"]
# The full version, including alpha/beta/rc tags.
release = metadata["version"]

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
#html_theme = 'sphinx_rtd_theme'
#html_theme = 'furo'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# -- Options for PDF output --------------------------------------------------

latex_engine = 'pdflatex'
