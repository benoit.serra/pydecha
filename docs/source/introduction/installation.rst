Installation
************
 
Cloning from gitlab
===================

Using http
----------

.. code-block:: bash

  git clone https://gitlab.com/benoit.serra/pydecha.git pydecha



Using ssh
---------

.. code-block:: bash

  git clone git@gitlab.com:benoit.serra/pydecha.git

.. warning::

  Cloning through ssh will not work because no key was generated yet

Python requirements
===================

Using pip
---------

.. code-block:: bash
  
  pip install -r pydecha_pipfreeze.txt

Using conda
-----------

.. code-block:: bash

  conda create -n pydecha --file pydecha_condaexport.txt
  # waiting for installation
  conda activate pydecha 
