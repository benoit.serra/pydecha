Tutorial for new module
***********************

Creating a new module
=====================

In order to create a new module, you can use the creation module mode
(cm). This module will allow you to create a module in a specific pipeline
(either CCD or CMOS) with every file needed for acquisition, analysis and
reporting.

.. code-block:: bash

	python pydecha/run.py cm {pipeline} {modulename}

:param pipeline: CCD or CMOS
:param modulename: name of the module (with normal chars)

In our example we will create a new module in the CMOS pipeline and then
integrate it in PYDECHA.

.. code-block:: bash

	$ python pydecha/run.py cm cmos demo
	Module demo created in <PYDECHAPATH>/pydecha/pydecha/modules/cmos/. 

This will create a new module in pydecha/modules/cmos/demo/.

Modify the analysis module
==========================

Opening the module located in pydecha/modules/cmos/demo_analysis/demo_analysis.py.

The module inherit from the analysis class which allows to define common
methods and arguments between all analysis modules, such as:

:self.logger: is the logger where the information can be written
:self.module_parameters: the full dictionnary containing module and general information

.. .. function:: self.get_dataset_info(filelist: list, file_type=str [FITS], save=bool)

..     	extract the metadata from a list of file to analyse (FITS only for now)
..     	the save boolean allows to save the metadata in a xlsx spreadsheet
..     	creating a new sheet for each module using this method.
    
.. .. function:: self.save_fig(fig: matplotlib.Figure, figure_name: str)

..     	will save YYYYMMDD_HHMMSS_module-figure_name in the outputs in png
    
.. .. function:: self.save_param(name: str, value)

..     	will save value for name in the 'output' field of the dictionnary
    
.. .. function:: self.save_map(array: numpy.ndarray, metadata=dict, file_type=str [FITS]) 

..     	will save the array into a fits file with the metadata in the header

.. .. function:: self.clean_output()

..     	will clean the dictionnary so that it can be saved without duplicating the general
..     	information (usually in the return of the start() method)

.. automodule:: classes.analysis_module
   :members:

------------------------------------------------------------------------

.. code-block:: python

	# -*- coding: utf-8 -*-
	"""
	Some docstring about the module
	"""
	# IMPORTS
	#########################
	# PYDECHA specific modules and classes
	import methods.plot_methods as plotm
	from classes.analysis_module import analysis
	# Standard modules
	import matplotlib.pyplot as plt
	import glob as glob
	import numpy as np


	# METHODS & CLASSES
	#########################
	class module(analysis):
		"""Example class with documented types

		:param None: analysis module initialize with the parameters from the yaml conf. file

		:return None: The return value. Nothing for now...
		"""
		def start(self):
			""""""
			# logger is the logger initialized in analysis_module.analysis
			# depending of the severity of the error the user can use:
			# logger.info/warning/error/critical
			logger = self.logger
			logger.info('Starting _TEMPLATE analysis...')
			# _TEMPLATE code goes here
			print('_TEMPLATE analysis module')
			print('_TEMPLATE parameters')
			# self.module_parameters is a dictionnary containing the module information
			# that were written in the .yaml configuration file
			print(self.module_parameters)
			file_list = glob.glob(self.module_parameters+'*.fits')
			self.get_dataset_info(file_list,
								  file_type='FITS',
								  save=True)

			# _TEMPLATE analysis
			# Creating an empty dataset
			data_array = np.zeros((1000, 1000))
			# Creating a figure
			fig, ax = plt.subplots(1, 1)
			# saving a parameter can be done using self.save_param
			self.save_param('_TEMPLATE variable name', 'Variable value')
			# saving a figure in
			# 'outputs/YYYYMMDD_DETECTOR/YYYYMMDD__TEMPLATE-figurename.png'
			self.save_fig(fig, 'figurename')
			# saving a map to FITS, all information in module_parameters will
			# be put in the header
			self.save_map(data_array,
						  metadata=self.module_parameters, file_type='FITS')

			logger.info('Ending _TEMPLATE analysis...')
			# if self.information was updated (for example using self.save_param)
			# returning it will save this info in the output yaml file.
			return self.clean_output()


	# MAIN
	#########################
	if __name__ == "__main__":
		pass

Each python analysis module inherits from analysis_module class with a start method.


Adding a new module to PYDECHA
==============================

Once the module is created, you will need to create a new configuration
file (demo_config.yaml).

.. code-block:: yaml

	################
	# pydecha configuration file for characterization
	# Used and modified by Benoit
	# 21/06/2019 - Creation date
	################

	general:
	  date_fmt: '%Y-%m-%d_%H-%M-%S'
	  author: 'Hari Seldon'
	  setup: 'setupname'
	  detector: 'detectornumber'
	  version: ''
	  savepath: 'outputs/'

	workflow:
	  type: 'cmos'
	  list:
		# module_name:[acquisition?, analysis?, reporting?]
		demo: [False,True,False]

	acquisition:
	  template:
		path: 'modules/template/template_acquisition.sh'
	  list:
		demo: ''

	analysis:
	  template:
		path: 'modules/template/template_analysis/template_pythonscript.py'
	  list:
		demo:
		  module_name: !!python/module:demo_analysis
		  data_path: 'pathtodata'
		  module_params:
			param1: 2
			param2:
			  a: 'string'
			  b: 3.14
		  output:


	report:
	  type: 'characterization'
	  template:
		path: 'modules/template/template_comments.yml'
	  list:
		demo: ''


Running the new pipeline
========================

.. code-block:: bash

	& python pydecha/run.py char cmos demo_config.yaml

	demo analysis module
	100%|############| 10000/10000 [00:00<00:00, 3329341.17it/s]
	WARNING: VerifyWarning: Keyword name 'data_path' is greater than 8 characters or contains characters not allowed by the FITS standard; a HIERARCH card will be created. [astropy.io.fits.card]

.. code-block:: bash

	& python pydecha/run.py -vvv char cmos demo_config.yaml

	2019-07-21 10:11:50,092 - pydecha_pipelines -                          start     Starting the pipelines...
	2019-07-21 10:11:50,128 - pydecha_pipelines -                          start      --------- demo ---------
	2019-07-21 10:11:50,160 - pydecha_pipelines -                          start     <bound method pipelines.acquisition of <classes.pipelines.pipelines object at 0x000001A4F1BA7390>> set to False
	2019-07-21 10:11:50,189 - pydecha_pipelines -                          start     <bound method pipelines.analysis of <classes.pipelines.pipelines object at 0x000001A4F1BA7390>>
	2019-07-21 10:11:50,220 - pydecha_pipelines -                       analysis     Analysis module starts...
	2019-07-21 10:11:50,248 - demo -                       __init__          Initializing analysis object for demo
	2019-07-21 10:11:50,429 - demo -                          start          Starting demo analysis...
	demo analysis module
	100%|############| 10000/10000 [00:00<00:00, 3329341.17it/s]
	WARNING: VerifyWarning: Keyword name 'data_path' is greater than 8 characters or contains characters not allowed by the FITS standard; a HIERARCH card will be created. [astropy.io.fits.card]
	2019-07-21 10:12:13,018 - astropy -                   _showwarning       VerifyWarning: Keyword name 'data_path' is greater than 8 characters or contains characters not allowed by the FITS standard; a HIERARCH card will be created.
	2019-07-21 10:12:21,323 - demo -                          start          Ending demo analysis...
	2019-07-21 10:12:21,563 - pydecha_pipelines -                       analysis     Analysis module ends...
	2019-07-21 10:12:21,598 - pydecha_pipelines -                          start     <bound method pipelines.report of <classes.pipelines.pipelines object at 0x000001A4F1BA7390>> set to False
	2019-07-21 10:12:23,450 - pydecha -                       <module>       PYDECHA, version v0.2-63-g02cda26
	2019-07-21 10:12:23,484 - pydecha -                       <module>       Executed in 57.84096312522888
