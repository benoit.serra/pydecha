cosmetics
*********

.. automodule:: cmos.cosmetics.cosmetics_acquisition
   :members:
.. automodule:: cmos.cosmetics.cosmetics_analysis
   :members:
