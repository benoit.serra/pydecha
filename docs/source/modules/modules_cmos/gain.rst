gain
****

.. automodule:: cmos.gain.gain_acquisition
   :members:
.. automodule:: cmos.gain.gain_analysis
   :members:
