cellgain
********

.. automodule:: cmos.cellgain.cellgain_acquisition
   :members:
.. automodule:: cmos.cellgain.cellgain_analysis
   :members:
