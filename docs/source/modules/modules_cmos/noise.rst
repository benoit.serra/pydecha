noise
*****

.. automodule:: cmos.noise.noise_acquisition
   :members:
.. automodule:: cmos.noise.noise_analysis
   :members:
