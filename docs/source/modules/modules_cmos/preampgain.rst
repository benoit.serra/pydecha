preampgain
**********

.. automodule:: cmos.preampgain.preampgain_acquisition
   :members:
.. automodule:: cmos.preampgain.preampgain_analysis
   :members:
