Change log
***********

.. _changelog:

:v0.1: Embedding pyqtgraph in the interface, communication tab with each
       instrument

------------

:v0.2: Automation of the pipelines with the blocks/modules

------------

:v0.3: - Add three modes of execution
           - Create Module [cm] to create a module (with the three blocks)
           - Documentation [doc] to manage documentation using a yaml configuration file
           - Characterisation [char] to start the pipelines with a yaml configuration file
       - Can run on Windows and Unix machines
       - logger can be printed to screen AND saved to the output
       - [cm] Create a template that is used to create a new module
       - [cm] Module creation takes a detector type argument dt:[cmos, ccd] to
         place the module in the correct location
       - [doc] Documentation takes a action type argument du to update the current
         documentation with the modules described in the yaml configuration file
       - [doc] adding a tutorial for a first analysis module creation
       - [char] Create analysis common block, all analysis module inherit from this
           - *save_param* to save a parameter in the analysis into the final yaml file
           - *save_fig* to save a mpl figure in a png file with possibility to tag it
           - *save_map* to save a numpy array to a FITS file with possibility to add metadata
           - *get_info_dataset* to get header info from a list of FITS, possibility to save it to excel
       - Pipelines can be re-run using a yaml file taken from outputs folder
