Authors
***********

Main developer
--------------

Benoit Serra - European Southern Observatory

Contributors
------------

Domingo Alvarez - European Southern Observatory
Elizabeth Georges - European Southern Observatory
