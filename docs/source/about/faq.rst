FAQ
***********

Error during installation because of mkl-fft/mkl-random
=======================================================

https://github.com/Diego999/pyGAT/issues/9

.. code-block:: bash
  
  & pip freeze > requirements.txt
  ...
  & pip install -r requirements.txt
  ...
  ERROR: Could not find a version that satisfies the requirement mkl-fft==1.0.12 (from -r pydecha_pipfreeze.txt (line 17)) (from versions: 1.0.0.17, 1.0.2, 1.0.6)
  ERROR: No matching distribution found for mkl-fft==1.0.12 (from -r pydecha_pipfreeze.txt (line 17))
  
.. code-block:: bash
  
  & pip freeze > requirements.txt
  ...
  & pip install -r requirements.txt
  ...  
  ERROR: Could not find a version that satisfies the requirement mkl-random==1.0.2 (from -r pydecha_pipfreeze.txt (line 18)) (from versions: 1.0.0.8, 1.0.1, 1.0.1.1)
  ERROR: No matching distribution found for mkl-random==1.0.2 (from -r pydecha_pipfreeze.txt (line 18))
  
Using pip freeze list the modules as well as their version. For those two
modules, one way to solve the issue is to remove the version number:

.. code-block:: bash

  mkl-fft==1.0.12
  mkl-random==1.0.2

.. code-block:: bash
   
  mkl-fft
  mkl-random
  
Error during the YAML configuration file import
===============================================


.. code-block:: bash

  --- Logging error ---
  Traceback (most recent call last):
    File "/SHARE/raid/users/bserra/Softwares/data_analysis/pydecha/pydecha/methods/pydecha_methods.py", line 51, in read_yamlreport
      yaml_report = yaml.load(stream, Loader=yaml.FullLoader)
    File "/SHARE/raid/users/bserra/anaconda3/envs/pydecha/lib/python3.6/site-packages/yaml/__init__.py", line 114, in load
      return loader.get_single_data()
    File "/SHARE/raid/users/bserra/anaconda3/envs/pydecha/lib/python3.6/site-packages/yaml/constructor.py", line 43, in get_single_data
      return self.construct_document(node)
    File "/SHARE/raid/users/bserra/anaconda3/envs/pydecha/lib/python3.6/site-packages/yaml/constructor.py", line 52, in construct_document
      for dummy in generator:
    File "/SHARE/raid/users/bserra/anaconda3/envs/pydecha/lib/python3.6/site-packages/yaml/constructor.py", line 404, in construct_yaml_map
      value = self.construct_mapping(node)
    File "/SHARE/raid/users/bserra/anaconda3/envs/pydecha/lib/python3.6/site-packages/yaml/constructor.py", line 210, in construct_mapping
      return super().construct_mapping(node, deep=deep)
    File "/SHARE/raid/users/bserra/anaconda3/envs/pydecha/lib/python3.6/site-packages/yaml/constructor.py", line 135, in construct_mapping
      value = self.construct_object(value_node, deep=deep)
    File "/SHARE/raid/users/bserra/anaconda3/envs/pydecha/lib/python3.6/site-packages/yaml/constructor.py", line 94, in construct_object
      data = constructor(self, tag_suffix, node)
    File "/SHARE/raid/users/bserra/anaconda3/envs/pydecha/lib/python3.6/site-packages/yaml/constructor.py", line 558, in construct_python_module
      return self.find_python_module(suffix, node.start_mark)
    File "/SHARE/raid/users/bserra/anaconda3/envs/pydecha/lib/python3.6/site-packages/yaml/constructor.py", line 518, in find_python_module
      "module %r is not imported" % name, mark)
  yaml.constructor.ConstructorError: while constructing a Python module
  module 'monitoring_analysis' is not imported
    in "config/cmos_config-dsraid2.yaml", line 66, column 20

  During handling of the above exception, another exception occurred:

  Traceback (most recent call last):
    File "/SHARE/raid/users/bserra/anaconda3/envs/pydecha/lib/python3.6/logging/__init__.py", line 994, in emit
      msg = self.format(record)
    File "/SHARE/raid/users/bserra/anaconda3/envs/pydecha/lib/python3.6/logging/__init__.py", line 840, in format
      return fmt.format(record)
    File "/SHARE/raid/users/bserra/anaconda3/envs/pydecha/lib/python3.6/logging/__init__.py", line 577, in format
      record.message = record.getMessage()
    File "/SHARE/raid/users/bserra/anaconda3/envs/pydecha/lib/python3.6/logging/__init__.py", line 338, in getMessage
      msg = msg % self.args
  TypeError: not all arguments converted during string formatting
  Call stack:
    File "pydecha/run.py", line 208, in <module>
      opts_prog.func(opts_prog)
    File "pydecha/run.py", line 95, in characterization
      setting_dict = pdcm.read_yamlreport('config/'+args.config)
    File "/SHARE/raid/users/bserra/Softwares/data_analysis/pydecha/pydecha/methods/pydecha_methods.py", line 53, in read_yamlreport
      logger.critical('YAML not opening, check YAML formatting', exc)
  Message: 'YAML not opening, check YAML formatting'
  Arguments: (ConstructorError('while constructing a Python module', <yaml.error.Mark object at 0x7f44b2e192b0>, "module 'monitoring_analysis' is not imported", <yaml.error.Mark object at 0x7f44b2e192b0>),)
  Traceback (most recent call last):
    File "pydecha/run.py", line 208, in <module>
      opts_prog.func(opts_prog)
    File "pydecha/run.py", line 95, in characterization
      setting_dict = pdcm.read_yamlreport('config/'+args.config)
    File "/SHARE/raid/users/bserra/Softwares/data_analysis/pydecha/pydecha/methods/pydecha_methods.py", line 57, in read_yamlreport
      return yaml_report
  UnboundLocalError: local variable 'yaml_report' referenced before assignment

