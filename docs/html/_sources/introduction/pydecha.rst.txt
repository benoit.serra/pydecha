PYDECHA
*******

Overview
========

PYthon DEtector CHAracterization


:Authors: Benoit Serra [ESO]  
:Version: 0.2

This documentation covers the setup and operation of the PYDECHA analysis
tool developed for the detector group (DESY) at ESO.
The goal is to provide an up-to-date documentation of validated modules
for the data analysis of detectors.

The idea behind the development of PYDECHA is to provide a framework for
the characterization that will allow the user to keep track of which pydecha
version had been used for a specific characterization run.

Actions
=======

PYDECHA consist of three different action blocks:

Acquisition 
  The acquisition block aims to offer a convenient python interface
  to interact with the Linux Local Control Unit (LLCU) of the New General
  Controller (NGC).
  This application is very specific to ESO needs and the goal is to provide
  a complete control and logging of every operation done with the NGC. 
  Another application is to automatize the process of characterization.
  This block is currently NOT tested and will be released in version 2.0
  of PYDECHA.

Analysis
  The analysis block aims to mimic a full pipelines of data reduction
  for data analysis of characterization data of the detectors.
  The block consist of a serie of analysis modules that will successively
  executed where the maximum of information of the process will be saved.
  This block is currently in tests and will be released in version 1.0
  of PYDECHA.

Reporting
  The reporting block aims to provide an automation tool for report 
  generation. This block will be highly dependent of the analysis results
  but some functions will offer a certain modularity in the process of
  report generation.
  All reporting output will be standardized for every detector.
  This block is currently NOT developed and will be released in version 3.0
  of PYDECHA.
