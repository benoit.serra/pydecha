Getting started
***************

Configuration
=============

The configuration file must follow a template describing the order and
the process which the pipeline will go through.

Below, we will describe the differnet field consisting the configuration file:

:general:

.. code-block:: yaml

	general:
	  date_fmt: '%Y-%m-%d_%H-%M-%S'
	  author: 'Hari Seldon'
	  setup: 'setupname'
	  detector: 'detectornumber'
	  version: ''
	  savepath: 'outputs/'

In the general field, entries like author, setup and detector must be filled.
The field 'version' will be field with the last git version using 'git describe --long'.

:workflow:

Within the workflow field, the user will describe which type of detector
he wants to use pydecha with. 
Then, the workflow/list entry will describe for each module if the
acquisition, analysis and reporting module are being used.
Not listing a module in workflow/list will simply not use it. Listing
a module with three False value will import it but skip it in the pipelines.
(good test for the libraries)

.. code-block:: yaml

	workflow:
	  # Information about the detector and pipeline to run (cmos)
	  detector:
		type: 'cmos'
		dimensions: 4096
		nb_outputs: 64
		read_freq: 100000
	  # List of the modules that will run and which block will be
	  # executed
	  list:
		# module_name:[acquisition?, analysis?, reporting?]
		demo: [False,True,False]
	  # Using self.save_fig in analysis module allows the user to save
	  # a figure in png. tag: True enable the automatic information tag
	  # to the figure. tag: False to disable it (for all).
	  plots:
	    tag: True

:acquisition:

To be written in v2.0

:analysis:

The analysis module will use a set of data located in data_path to run
its algorithm.
:params module_name: yaml serializing of module demo_analysis
:params data_path: the path of the data that will be used for the analysis
:params module_params: parameters used for the module, no checking of the compatibility
of the parameters in respect to the module, be careful!
:params output: usually empty field that can be use to store important
value within the module

.. code-block:: yaml

	analysis:
	  template:
		path: 'modules/template/template_analysis/template_pythonscript.py'
	  list:
		demo:
		  module_name: !!python/module:demo_analysis
		  module_params:
   		    data_path: 'pathtodata'
			param1: 2
			param2:
			  a: 'string'
			  b: 3.14
		  output:

:reporting:

To be written in v3.0

Modes
=====

PYDECHA runs three different modes:

[doc] documentation
-------------------
  Update current documentation using demo_config.yaml config file
    
    .. code-block:: bash
    
      python pydecha/run.py -vvv doc cmos -c demo_config.yaml -du
      cd docs/
      make html
      
  Alternatively, you can run the following command line from pydecha root folder:
  
    .. code-block:: bash
    
      sphinx-build docs/source docs/html
      
  .. note::
    
     The documentation will be then available in the following directory:
     
     .. code-block:: bash
        
        [pydecha_folder]/docs/html/index.html

[char] characterization
-----------------------
  Run characterization for cmos type using demo_config.yaml

    .. code-block:: bash
    
      python pydecha/run.py -vvv char cmos -c demo_config.yaml

  Run characterization for ccd type using demo_config.yaml

    .. code-block:: bash
    
      python pydecha/run.py -vvv char ccd -c TBD.yaml

[cm] module creation
--------------------
  Creating a new module for cmos like sensor:

    .. code-block:: bash
    
      python pydecha/run.py -vvv cm cmos demomodule

  Creating a new module for ccd:

    .. code-block:: bash
        
      python pydecha/run.py -vvv cm ccd demomodule
